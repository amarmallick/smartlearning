﻿using System;

namespace ConnectSC.Data.ViewModels
{
    public class ServiceTicketCommentsModel
    {
        public int ID { get; set; }
        public Nullable<int> ServiceTicketID { get; set; }
        public string Remarks { get; set; }
        public Nullable<System.DateTime> RemarksDate { get; set; }
        public int UserID { get; set; }
        public string FullName { get; set; }
    }
}
