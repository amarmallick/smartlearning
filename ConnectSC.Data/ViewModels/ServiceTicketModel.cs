﻿using ConnectSC.Data.Models;
using System;
using System.Collections.Generic;

namespace ConnectSC.Data.ViewModels
{
    public class ServiceTicketViewModel
    {
        public ServiceTicketViewModel()
        {
            ServiceTicketComments = new List<ServiceTicketCommentsModel>();
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public string AttachmentPath { get; set; }
        public DateTime? TicketDate { get; set; }
        public List<ServiceTicketCommentsModel> ServiceTicketComments { get; set; }
    }
}
