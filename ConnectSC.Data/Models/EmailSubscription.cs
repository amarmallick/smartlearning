//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ConnectSC.Data.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class EmailSubscription
    {
        public int SubsciptionID { get; set; }
        public string Email { get; set; }
        public int PhoneNumber { get; set; }
    }
}
