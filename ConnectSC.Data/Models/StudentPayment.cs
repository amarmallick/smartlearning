//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ConnectSC.Data.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class StudentPayment
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Status { get; set; }
        public decimal FeePaid { get; set; }
        public decimal FeeDue { get; set; }
        public decimal TotalFee { get; set; }
        public string PaidMode { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
    
        public virtual User User { get; set; }
    }
}
