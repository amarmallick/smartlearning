﻿namespace ConnectSC.Models
{
    public class CareerModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Position { get; set; }
        public string Experience { get; set; }
        public string Designation { get; set; }
        public string AttachmentPath { get; set; }
    }
}
