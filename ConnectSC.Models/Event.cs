﻿using System;

namespace ConnectSC.Models
{
    public class Event
    {
        public string id { get; set; }
        public string title { get; set; }
        public bool allDay { get; set; }
        public DateTime start { get; set; }
        public DateTime? end { get; set; }
        public string url { get; set; }
    }
}
