﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConnectSC.Models
{
  public class ServiceEnquiry
    {
        public int EnquiryId { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string Service { get; set; }

        public string Message { get; set; }
    }

    public class ServiceEnquiryList
    {
        public List<ServiceEnquiry> ServiceList { get; set; }
    }

}
