﻿using FluentValidation;
using FluentValidation.Attributes;

namespace ConnectSC.Models
{
    [Validator(typeof(ChangePwdModelValidator))]
    public class ChangePwdModel
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public string UserKey { get; set; }
    }

    public class ChangePwdModelValidator : AbstractValidator<ChangePwdModel>
    {
        public ChangePwdModelValidator()
        {
            RuleFor(x => x.OldPassword).NotEmpty().WithMessage("Old Password cannot be blank.");

            RuleFor(x => x.NewPassword).NotEmpty().WithMessage("New Password cannot be blank.")
                                          .Length(0, 50).WithMessage("New Password cannot be more than 50 characters.");
        }
    }
}
