﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConnectSC.Models
{
   public class StudentTeacherModel
    {
        public int userId { get; set; }
        public List<int> userIds { get; set; }
    }
}
