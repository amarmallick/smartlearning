﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConnectSC.Models
{
	public class SubmitAssignment
	{
		public int Id { get; set; }

		public int UserId { get; set; }

		public string Attachment { get; set; }

		public Nullable<int> CreatedBy { get; set; }

		public Nullable<System.DateTime> CreatedDate { get; set; }

		public string Comments { get; set; }
	}
}
