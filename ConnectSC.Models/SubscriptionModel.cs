﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConnectSC.Models
{
    public class SubscriptionModel
    {

        public int SubscriptionId { get; set; }

        public string SubcriptionName { get; set; }

        public decimal SubscriptionPrice { get; set; }

        public string CreatedBy { get; set; }

        public Nullable<System.DateTime> CreatedDate { get; set; }

    }
}
