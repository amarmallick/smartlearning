﻿using System;

namespace ConnectSC.Models
{
    public class AssignmetModel
    {
        public int AssignmentID { get; set; }
        public string AssignmentTitle { get; set; }
        public string Description { get; set; }
        public DateTime? DueDate { get; set; }
        public string Attachment { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int ModifiedBy { get; set; }
        public int ModifiedOn { get; set; }
        public string CreatedByName { get; set; }
        public int Marks { get; set; }
    }
}
