﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConnectSC.Models
{
   public class UserSubscriptionModel
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public int SubscriptionId { get; set; }

        public Nullable<DateTime> SubscriptionStartDate { get; set; }

        public Nullable<DateTime> SubscriptionEndDate { get; set; }

        public string CreatedBy { get; set; }

        public Nullable<System.DateTime> CreatedDate { get; set; }


        public string username { get; set; }

      
        public string subcriptionname { get; set; }
        public string[] Tutors { get; set; }
    }
}
