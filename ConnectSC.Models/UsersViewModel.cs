﻿using ConnectSC.Models;
using System.Collections.Generic;

namespace ConnectSmartConsulting.Models
{
    public class UsersViewModel
    {
        public List<Users> Users { get; set; }
        public List<SubscriptionModel> Subscription { get; set; }
        public List<UserSubscriptionModel> UserSubscriptionDetails { get; set; }
        public List<TutorModel> Tutors { get; set; }
        public List<TutorModel> Students { get; set; }
    }
}
