﻿namespace ConnectSC.Models
{
    public class CounterModel
    {
        public short NewUsers { get; set; }
        public short Students { get; set; }
        public short Tutors { get; set; }
        public short NewSessions { get; set; }
    }
}
