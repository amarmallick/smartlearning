﻿using System.Collections.Generic;

namespace ConnectSmartConsulting.Models
{
    public class ContactDetails
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public List<string> Service { get; set; }
        public string SelectedService { get; set; }
        public string Phone { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
    }
}
