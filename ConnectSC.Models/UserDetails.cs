﻿namespace ConnectSmartConsulting.Models
{
    public class UserDetails
    {
        public int UserID { get; set; }
        public string Email { get; set; }
        public string Pass { get; set; }
        public string HighestEducation { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public int Role { get; set; }
        public string CurrentCity { get; set; }
        public string AreasYouTeach { get; set; }
        public string SubjectYouTeach { get; set; }
        public string TimeAvailibility { get; set; }
        public string EducationBoard { get; set; }
        public string Comments { get; set; }
        public string ApprovedStatus { get; set; }
        public string ActiveStatus { get; set; }
        public bool IsActive { get; set; }
        public bool IsApproved { get; set; }
        public string ImagePath { get; set; }
        public string ClassName { get; set; }
    }

    public class AproveUser
    {
        public int UserID { get; set; }
        public bool IsApproved { get; set; }
        public string Reason { get; set; }
    }
}
