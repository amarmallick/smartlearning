﻿namespace ConnectSC.Models
{
    public enum URole
    {
        Admin = 1,
        Tutor = 2,
        Student = 3
    }
}
