﻿using System;

namespace ConnectSC.Models
{
    public class MeetingModel
    {
        public int MeetingID { get; set; }
        public int TutorID { get; set; }
        public int StudentID { get; set; }
        public string StudentName { get; set; }
        public string TutorName { get; set; }
        public string MeetingName { get; set; }
        public string MeetingUrl { get; set; }
        public DateTime? StartDateTime { get; set; }
        public DateTime? EndDateTime { get; set; }
    }
}
