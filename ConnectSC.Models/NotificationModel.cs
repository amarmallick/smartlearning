﻿using System;

namespace ConnectSC.Models
{
    public class NotificationModel
    {
        public int ID { get; set; }
        public string UserGroup { get; set; }
        public string NotificationText { get; set; }
        public System.DateTime ShowFrom { get; set; }
        public Nullable<System.DateTime> ShowTill { get; set; }
        public bool IsRead { get; set; }
    }
}
