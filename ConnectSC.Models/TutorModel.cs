﻿namespace ConnectSC.Models
{
    public class TutorModel
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string HighestEducation { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string CurrentCity { get; set; }
        public string AreasYouTeach { get; set; }
        public string SubjectYouTeach { get; set; }
        public string TimeAvailibility { get; set; }
        public string EducationBoard { get; set; }
        public string FullName { get; set; }
        public string ImagePath { get; set; }
    }
}
