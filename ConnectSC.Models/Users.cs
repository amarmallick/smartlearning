﻿using ConnectSC.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ConnectSmartConsulting.Models
{
    public class Users
    {
        public int UserId { get; set; }

        [Required]
        public string UserName { get; set; }
        public string Password { get; set; }
        public int Role { get; set; }

        public List<UserDetails> UserDetails { get; set; }
        public bool IsApproved { get; set; }
        public bool IsActive { get; set; }
        public string ApprovedStatus { get; set; }
        public string ActiveStatus { get; set; }
        public string SubscriptionStartDate { get; set; }
        public string SubscriptionPackage { get; set; }

        public string SubscriptionEndDate { get; set; }
        public List<TutorModel> TutorList { get; set; }
    }

    public class LoggedInUser
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public bool IsActive { get; set; }
        public bool IsApproved { get; set; }
        public int Role { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ImagePath { get; set; }
        public string UserKey { get; set; }
    }
}
