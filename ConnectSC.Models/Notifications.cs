﻿using System;

namespace ConnectSC.Models
{
    public class Notifications
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public string NotficationText { get; set; }
        public DateTime? NotificationDate { get; set; }
        public bool IsRead { get; set; }
    }
}
