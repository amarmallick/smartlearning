﻿using ConnectSC.Data.Models;
using ConnectSC.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;

namespace TutorApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class TutorsController : ApiController
    {
        private TutionProEntities tutionProEntities = new TutionProEntities();

        [HttpGet]
        [ActionName("search")]
        public IHttpActionResult SearchTutors()
        {
            using (var context = new TutionProEntities())
            {
                //var param = new SqlParameter[]
                //{
                //    new SqlParameter("@CustomerID", id)
                //};
                IEnumerable<TutorModel> custSub = context.Database
                    .SqlQuery<TutorModel>("exec [dbo].[SearchTutors] ");

                return Ok(custSub.ToList());
            }
        }

        [HttpGet]
        [ActionName("get-all")]
        public IHttpActionResult GetAll()
        {
            using (var context = new TutionProEntities())
            {
                var list = (from p in tutionProEntities.Users
                            join c in tutionProEntities.UserDetails on p.UserId equals c.UserID into g
                            from result in g.DefaultIfEmpty()// Left join
                            where p.Role == 2
                            select new TutorModel
                            {
                                UserId = result.UserID,
                                UserName = p.UserName,
                                FirstName = result.FirstName,
                                LastName = result.LastName,
                                FullName = result.FirstName + " " + result.LastName,
                                HighestEducation = result.HighestEducation,
                                CurrentCity = result.CurrentCity
                            }
             ).OrderBy(c => c.FullName).ToList();

                return Ok(list);
            }
        }
    }
}