﻿using System.Web.Mvc;

namespace TutorApi.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //ViewBag.Title = "Home Page";
            return RedirectToAction("index","swagger");
        }
    }
}
