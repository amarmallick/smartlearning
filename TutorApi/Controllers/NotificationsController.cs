﻿using ConnectSC.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Linq;
using System.Data.SqlClient;
using ConnectSC.Models;

namespace TutorApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class NotificationsController : ApiController
    {
        [ActionName("add")]
        public IHttpActionResult Add(NotificationModel notificationModel)
        {
            try
            {
                Notification notification = new Notification();
                using (TutionProEntities context = new TutionProEntities())
                {
                    SqlParameter[] param = new SqlParameter[] {
                        new SqlParameter("@NotificationText", notificationModel.NotificationText),
                        new SqlParameter("@ShowFrom", notificationModel.ShowFrom),
                        new SqlParameter("@ShowTill", notificationModel.ShowTill),
                        new SqlParameter("@UserGroup", notificationModel.UserGroup)
                    };
                    context.Database.ExecuteSqlCommand
                        ("exec [AddNotification] @NotificationText, @ShowFrom, @ShowTill, @UserGroup",
                        param);
                    context.SaveChanges();
                    return Ok("success");
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [ActionName("get-all")]
        public IHttpActionResult GetAll()
        {
            using (var context = new TutionProEntities())
            {
                var list = context.Notifications.ToList().Select(i => new { i.NotificationText, i.ShowFrom, i.ShowTill, i.NGKey }).Distinct();
                return Ok(list.ToList());
            }
        }

        [HttpPost]
        [ActionName("get")]
        public IHttpActionResult GetNotifications(string userKey)
        {
            using (var context = new TutionProEntities())
            {
                var param = new SqlParameter[]
                {
                    new SqlParameter("@UserKey", userKey)
                };
                IEnumerable<Notification> custSub = context.Database
                    .SqlQuery<Notification>("exec [dbo].[GetNotifications] @UserKey", param);

                return Ok(custSub.ToList());
            }
        }

        [HttpPost]
        [ActionName("delete")]
        public IHttpActionResult Delete(string ngKey)
        {
            using (var context = new TutionProEntities())
            {
                var entity = context.Notifications.Where(c=>c.NGKey.ToString().Equals(ngKey)).FirstOrDefault();
                if (entity == null)
                {
                    return NotFound();
                }

                context.Notifications.Remove(entity);
                context.SaveChanges();
                return Ok(entity);
            }
        }
    }
}
