﻿using System.Data.Entity.Migrations;
using ConnectSC.Data.Models;
using ConnectSC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace TutorApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class StudentsController : ApiController
    {
        private TutionProEntities tutionProEntities = new TutionProEntities();

        [HttpGet]
        [Route("calendar-stu")]
        public HttpResponseMessage GetCalendarStudent(HttpRequestMessage request, DateTime start, DateTime end, int studentId)
        {
            //  FullCalendar will keep passing the start and end values in as you navigate through the calendar pages
            //  You should therefore use these days to determine what events you should return . Ie
            //  i.e. var events = db.Events.Where(event => event.Start > start && event.End < end);

            //tutionProEntities.Meetings.Where(c => c.StudentID == studentId && c.StartDateTime > start && c.EndDateTime < end).ToList();
            var list = (from p in tutionProEntities.Meetings
                               .OrderByDescending(c => c.EndDateTime)
                            //.Skip(currentPage * currentPageSize)
                            //.Take(currentPageSize) // Take pageSize elements
                        join c in tutionProEntities.UserDetails on p.TutorID equals c.UserID into g
                        from result in g.DefaultIfEmpty()// Left join
                        where p.StudentID == studentId
                        select new
                        {
                            p.ID,
                            p.MeetingName,
                            p.MeetingUrl,
                            p.StartDateTime,
                            p.EndDateTime,
                            result.FirstName,
                            result.LastName
                        }
               ).ToList();

            var events = new List<Event>();

            foreach (var item in list)
            {
                events.Add(new Event
                {
                    id = item.ID.ToString(),
                    title = string.Format("{0} with {1} {2}", item.MeetingName, item.FirstName, item.LastName),
                    allDay = false,
                    start = item.StartDateTime.Value,
                    end = item.EndDateTime.Value,
                    url = item.MeetingUrl
                });
            }

            var response = request.CreateResponse(HttpStatusCode.OK, events);
            return response;
        }

        [HttpGet]
        [Route("calendar-tut")]
        public HttpResponseMessage GetCalendarTutor(HttpRequestMessage request, DateTime start, DateTime end, int tutorId)
        {
            //  FullCalendar will keep passing the start and end values in as you navigate through the calendar pages
            //  You should therefore use these days to determine what events you should return . Ie
            //  i.e. var events = db.Events.Where(event => event.Start > start && event.End < end);

            //tutionProEntities.Meetings.Where(c => c.StudentID == studentId && c.StartDateTime > start && c.EndDateTime < end).ToList();
            var list = (from p in tutionProEntities.Meetings
                               .OrderByDescending(c => c.EndDateTime)
                            //.Skip(currentPage * currentPageSize)
                            //.Take(currentPageSize) // Take pageSize elements
                        join c in tutionProEntities.UserDetails on p.TutorID equals c.UserID into g
                        from result in g.DefaultIfEmpty()// Left join
                        where p.TutorID == tutorId
                        select new
                        {
                            p.ID,
                            p.MeetingName,
                            p.MeetingUrl,
                            p.StartDateTime,
                            p.EndDateTime,
                            result.FirstName,
                            result.LastName
                        }
               ).ToList();

            var events = new List<Event>();

            foreach (var item in list)
            {
                events.Add(new Event
                {
                    id = item.ID.ToString(),
                    title = string.Format("{0} with {1} {2}", item.MeetingName, item.FirstName, item.LastName),
                    allDay = false,
                    start = item.StartDateTime.Value,
                    end = item.EndDateTime.Value,
                    url = item.MeetingUrl
                });
            }

            var response = request.CreateResponse(HttpStatusCode.OK, events);
            return response;
        }

        [HttpGet]
        [ActionName("get-all")]
        public IHttpActionResult GetAll()
        {
            using (var context = new TutionProEntities())
            {
                var list = (from p in tutionProEntities.Users
                            join c in tutionProEntities.UserDetails on p.UserId equals c.UserID into g
                            from result in g.DefaultIfEmpty()// Left join
                            where p.Role == 3
                            select new TutorModel
                            {
                                UserId = result.UserID,
                                UserName = p.UserName,
                                FirstName = result.FirstName,
                                LastName = result.LastName,
                                FullName = result.FirstName + " " + result.LastName
                            }
             ).OrderBy(c => c.FullName).ToList();

                return Ok(list);
            }
        }

        [HttpGet]
        [ActionName("search")]
        public IHttpActionResult Search(string searchFilter)
        {
            using (var context = new TutionProEntities())
            {
                List<TutorModel> list;
                if (string.IsNullOrEmpty(searchFilter))
                {
                    list = (from p in tutionProEntities.Users
                            join c in tutionProEntities.UserDetails on p.UserId equals c.UserID into g
                            from result in g.DefaultIfEmpty()// Left join
                            where p.Role == 3
                            select new TutorModel
                                {
                                    UserId = result.UserID,
                                    UserName = p.UserName,
                                    FirstName = result.FirstName,
                                    LastName = result.LastName,
                                    FullName = result.FirstName + " " + result.LastName
                                }
                            ).OrderBy(c => c.FullName).ToList();
                }
                else
                {
                    searchFilter = searchFilter.ToLower();
                    list = (from p in tutionProEntities.Users
                            join c in tutionProEntities.UserDetails on p.UserId equals c.UserID into g
                            from result in g.DefaultIfEmpty()// Left join
                            where p.Role == 3
                            && (result.FirstName.ToLower().Contains(searchFilter) 
                            || result.LastName.ToLower().Equals(searchFilter))
                            select new TutorModel
                            {
                                UserId = result.UserID,
                                UserName = p.UserName,
                                FirstName = result.FirstName,
                                LastName = result.LastName,
                                FullName = result.FirstName + " " + result.LastName
                            }
                            ).OrderBy(c => c.FullName).ToList();
                }
                return Ok(list);
            }
        }

        [HttpGet]
        [ActionName("get-by-tutor")]
        public IHttpActionResult GetAll(int tutorId, int count = 0)
        {
            using (var context = new TutionProEntities())
            {
                var list = (from p in tutionProEntities.Users
                            join c in tutionProEntities.UserDetails on p.UserId equals c.UserID into g
                            from result in g.DefaultIfEmpty()// Left join
                            where p.Role == 3
                            select new TutorModel
                            {
                                UserId = result.UserID,
                                UserName = p.UserName,
                                FirstName = result.FirstName,
                                LastName = result.LastName,
                                FullName = result.FirstName + " " + result.LastName,
                                HighestEducation = result.HighestEducation,
                                CurrentCity = result.CurrentCity
                            }
             ).OrderBy(c => c.FullName).ToList();

                if (count > 0)
                    return Ok(list.OrderByDescending(c => c.UserId).Take(count).Skip(0));
                return Ok(list);
            }
        }

        [ActionName("SubmitAssignment")]
        public void SubmitAssignment(SubmittedAssignment smartySubmittedassignment)
        {
            try
            {
                using (var context = new TutionProEntities())
                {
                    context.SubmittedAssignments.AddOrUpdate(c => c.UserId, smartySubmittedassignment);
                    context.SaveChanges();

                    // Attached object tracks modifications automatically
                    //context.SaveChanges();
                    //context.Entry(userSubscription).State = EntityState.Added;
                    //context.SaveChanges();
                }
            }
            catch (Exception)
            {
                // log.Error(ex);
            }
        }
    }
}
