﻿using ConnectSC.Data.Models;
using ConnectSC.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace TutorApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MeetingsController : ApiController
    {
        private TutionProEntities tutionProEntities = new TutionProEntities();

        [HttpGet]
        [ActionName("get-all")]
        public IHttpActionResult GetAll()
        {
            using (var context = new TutionProEntities())
            {
                var list = (from p in tutionProEntities.Meetings
                            join c in tutionProEntities.UserDetails on p.TutorID equals c.UserID into g
                            join d in tutionProEntities.UserDetails on p.StudentID equals d.UserID into h
                            from result in g.DefaultIfEmpty()// Left join
                            //where p.Role == 2
                            select new MeetingModel
                            {
                                MeetingID = p.ID,
                                StudentID = p.StudentID,
                                StudentName = g.FirstOrDefault().FirstName + " " + g.FirstOrDefault().LastName,
                                TutorID = p.TutorID,
                                TutorName = h.FirstOrDefault().FirstName + " " + h.FirstOrDefault().LastName,
                                MeetingName = p.MeetingName,
                                MeetingUrl = p.MeetingUrl,
                                EndDateTime = p.EndDateTime,
                                StartDateTime = p.StartDateTime
                            }
                        ).OrderByDescending(c => c.StartDateTime).ToList();

                return Ok(list);
            }
        }

        [HttpPost]
        [ActionName("add")]
        public void Post(Meeting meeting)
        {
            using (TutionProEntities tutionProEntities = new TutionProEntities())
            {
                DbContextTransaction transaction = tutionProEntities.Database.BeginTransaction();

                try
                {
                    tutionProEntities.Meetings.Add(meeting);
                    tutionProEntities.SaveChanges();

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                }
            }
        }
    }
}
