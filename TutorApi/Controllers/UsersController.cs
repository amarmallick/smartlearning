﻿using ConnectSC.Data.Models;
using ConnectSC.Models;
using ConnectSmartConsulting.Models;
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Data.Entity.Migrations;

namespace TutorApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UsersController : ApiController
    {
        private TutionProEntities tutionProEntities = new TutionProEntities();

        [ActionName("get-all")]
        public List<User> Get()
        {
            return tutionProEntities.Users.ToList();
        }

        [ActionName("get")]
        public User Get(int id)
        {
            User user = tutionProEntities.Users.Find(id);
            if (user != null)
            {
                user.UserDetails.FirstOrDefault().ImagePath = user.UserDetails.FirstOrDefault().ImagePath ?? "no_image_available.jpeg";
            }
            return tutionProEntities.Users.Find(id);
        }

        [HttpGet]
        [ActionName("get-user")]
        public LoggedInUser GetByKey(string uk)
        {
            LoggedInUser loggedInUser = new LoggedInUser();
            User user = tutionProEntities.Users.Where(c => c.UserKey.ToString().Equals(uk)).FirstOrDefault();

            if (user != null)
            {
                var userDetails = tutionProEntities.UserDetails.Where(m => m.UserID == user.UserId).ToList();

                loggedInUser = new LoggedInUser
                {
                    UserId = user.UserId,
                    UserName = user.UserName,
                    IsActive = user.IsActive,
                    IsApproved = user.IsApproved,
                    Role = user.Role.Value,
                    FirstName = userDetails[0].FirstName,
                    LastName = userDetails[0].LastName,
                    ImagePath = user.UserDetails.FirstOrDefault().ImagePath ?? "no_image_available.jpeg",
                    UserKey = Convert.ToString(user.UserKey)
                };
            }
            return loggedInUser;
        }

        [HttpPost]
        [ActionName("authenticate")]
        public IHttpActionResult Authenticate(string username, string password)
        {
            LoggedInUser loggedInUser = new LoggedInUser();

            var user = tutionProEntities.Users.Where(
                 m => m.UserName.Equals(username)
                 && m.Password.Equals(password)
                 && m.IsActive == true).FirstOrDefault();

            if (user != null)
            {
                var userDetails = tutionProEntities.UserDetails.Where(m => m.UserID == user.UserId).ToList();

                loggedInUser = new LoggedInUser
                {
                    //UserId = user.UserId,
                    //UserName = user.UserName,
                    //IsActive = user.IsActive,
                    IsApproved = user.IsApproved,
                    //Role = user.Role.Value,
                    //FirstName = userDetails[0].FirstName,
                    //LastName = userDetails[0].LastName,
                    //ImagePath = user.UserDetails.FirstOrDefault().ImagePath ?? "no_image_available.jpeg",
                    UserKey = Convert.ToString(user.UserKey)
                };
                if (loggedInUser.IsApproved == false)
                {
                    return BadRequest("not_approved");
                }
            }
            else
            {
                return BadRequest("invalid");
            }
            return Ok(loggedInUser);
        }

        [HttpPost]
        [ActionName("post")]
        public void Post(UserDetails userDetails)
        {
            using (TutionProEntities tutionProEntities = new TutionProEntities())
            {
                DbContextTransaction transaction = tutionProEntities.Database.BeginTransaction();

                try
                {
                    var user = new User
                    {
                        UserName = userDetails.Email,
                        Password = userDetails.Pass,
                        Role = userDetails.Role,
                        IsActive = true,
                        IsApproved = userDetails.IsApproved,
                        CreatedDate = DateTime.UtcNow,
                        UserKey = Guid.NewGuid(),
                    };
                    tutionProEntities.Users.Add(user);
                    tutionProEntities.SaveChanges();

                    int userId = user.UserId;

                    var userDetail = new UserDetail
                    {
                        UserID = userId,
                        FirstName = userDetails.FirstName,
                        LastName = userDetails.LastName,
                        HighestEducation = userDetails.HighestEducation,
                        PhoneNumber = userDetails.PhoneNumber,
                        Address = userDetails.Address,
                        AreasYouTeach = userDetails.AreasYouTeach,
                        CurrentCity = userDetails.CurrentCity,
                        SubjectYouTeach = userDetails.SubjectYouTeach,
                        TimeAvailibility = userDetails.TimeAvailibility,
                        Comments = userDetails.Comments,
                        EducationBoard = userDetails.EducationBoard,
                        ClassName = userDetails.ClassName
                    };
                    tutionProEntities.UserDetails.Add(userDetail);
                    tutionProEntities.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                }
            }
        }

        [HttpPost]
        [ActionName("update-user")]
        public void Update(UserDetails userDetails)
        {
            using (TutionProEntities context = new TutionProEntities())
            {
                // Update User
                var userToUpdate = context.UserDetails.Where(c => c.UserID == userDetails.UserID).FirstOrDefault();

                if (userToUpdate != null)
                {
                    userToUpdate.UserID = userDetails.UserID;
                    userToUpdate.FirstName = userDetails.FirstName;
                    userToUpdate.LastName = userDetails.LastName;
                    userToUpdate.HighestEducation = userDetails.HighestEducation;
                    userToUpdate.PhoneNumber = userDetails.PhoneNumber;
                    userToUpdate.Address = userDetails.Address;
                    userToUpdate.AreasYouTeach = userDetails.AreasYouTeach;
                    userToUpdate.CurrentCity = userDetails.CurrentCity;
                    userToUpdate.SubjectYouTeach = userDetails.SubjectYouTeach;
                    userToUpdate.TimeAvailibility = userDetails.TimeAvailibility;
                    userToUpdate.Comments = userDetails.Comments;
                    userToUpdate.EducationBoard = userDetails.EducationBoard;
                    userToUpdate.ImagePath = userDetails.ImagePath;
                    userToUpdate.ClassName = userDetails.ClassName;

                    context.Entry(userToUpdate).State = EntityState.Modified;
                    context.SaveChanges();
                }
            }
        }

        [HttpPost]
        [ActionName("update")]
        public void Update(AproveUser approveUser)
        {
            using (TutionProEntities context = new TutionProEntities())
            {
                var user = context.Users.Find(approveUser.UserID);
                if (user != null)
                {
                    user.UserId = approveUser.UserID;
                    user.IsApproved = approveUser.IsApproved;

                    context.Entry(user).State = EntityState.Modified;
                    context.SaveChanges();
                }
            }
        }

        // DELETE: api/Users/5
        public void Delete(int id)
        {
        }

        [ActionName("postEnquiry")]
        public void postEnquiry(Enquiry serviceEnquiry)
        {
            try
            {
                using (TutionProEntities context = new TutionProEntities())
                {
                    var user = context.Enquiries;
                    context.Entry(serviceEnquiry).State = EntityState.Added;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                // log.Error(ex);
            }
        }

        [ActionName("getEnquiry")]
        public List<Enquiry> GetEnquiry()
        {
            using (TutionProEntities context = new TutionProEntities())
            {
                return context.Enquiries.ToList();
            }
        }

        [HttpGet]
        [ActionName("enquiry-details")]
        public Enquiry EnquiryDetails(int id)
        {
            using (TutionProEntities context = new TutionProEntities())
            {
                return context.Enquiries.Find(id);
            }
        }

        [ActionName("getSubscriptionPackage")]
        public List<Subscription> getSubscriptionPackage()
        {
            using (TutionProEntities context = new TutionProEntities())
            {
                return context.Subscriptions.ToList();
            }
        }

        [ActionName("getUserSubscriptionDetails")]
        public List<UserSubscriptionDetails_Result> getUserSubscriptionDetails()
        {
            using (TutionProEntities context = new TutionProEntities())
            {
                return context.UserSubscriptionDetails().ToList();
            }
        }

        [ActionName("AddUserSubscriptionDetails")]
        public void AddUserSubscriptionDetails(UserSubscription userSubscription)
        {
            try
            {
                using (TutionProEntities context = new TutionProEntities())
                {
                    var user = context.UserSubscriptions;

                    context.UserSubscriptions.AddOrUpdate(c => c.UserId, userSubscription);
                    context.SaveChanges();

                    // Attached object tracks modifications automatically
                    //context.SaveChanges();
                    //context.Entry(userSubscription).State = EntityState.Added;
                    //context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                // log.Error(ex);
            }
        }

        [HttpGet]
        [ActionName("has-permission")]
        public bool HasPermission(int id, string permission)
        {
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TutionPro"].ConnectionString))
            {
                SqlParameter[] param = new SqlParameter[] {
                    new SqlParameter("@UserId", id),
                    new SqlParameter("@Permission", permission)
                };
                var count = Convert.ToInt32(SqlHelper.ExecuteScalar(connection, CommandType.StoredProcedure, "[dbo].[HasPermission]", param));
                return count == 0 ? false : true;
            }
        }

        [ActionName("getStudentTeacherMapping")]
        public List<getStudentTeacherMapping_Result1> GetStudentTeacherMapping(int userId)
        {
            using (TutionProEntities context = new TutionProEntities())
            {
                return context.getStudentTeacherMapping(userId).ToList();
            }
        }

        [ActionName("addStudentTeacherMapping")]
        public List<getStudentTeacherMapping_Result1> AddStudentTeacherMapping(StudentTeacherModel studentTeacherModel)
        {
            var lovID = 0;
            //int userId, List<int> userIds
            using (var context = new TutionProEntities())
            {
                DbContextTransaction transaction = tutionProEntities.Database.BeginTransaction();
                try
                {
                    LovItem lovItem = new LovItem()
                    {
                        LovValue = studentTeacherModel.userId.ToString()
                    };

                    var lovObj = context.LovItems.Where(x => x.LovValue == studentTeacherModel.userId.ToString()).FirstOrDefault();
                    if (lovObj == null)
                    {
                        context.LovItems.Add(lovItem);
                        lovID = context.SaveChanges();
                        lovObj = context.LovItems.Where(x => x.LovValue == studentTeacherModel.userId.ToString()).FirstOrDefault();

                    }
                    lovID = lovObj.LovID;

                    var lovItemValueObj = context.LovItemValues.Where(x => x.LovID == lovID).ToList();
                    context.LovItemValues.RemoveRange(lovItemValueObj);
                    context.SaveChanges();

                    context.LovItems.Remove(lovObj);
                    context.SaveChanges();


                    context.Entry(lovItem).State = EntityState.Added;
                    context.SaveChanges();

                    foreach (var mappingId in studentTeacherModel.userIds)
                    {
                        LovItemValue lovItemValue = new LovItemValue()
                        {
                            LovID = lovItem.LovID,
                            LovItem = mappingId.ToString()

                        };
                        context.Entry(lovItemValue).State = EntityState.Added;
                        context.SaveChanges();

                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                }
                return context.getStudentTeacherMapping(studentTeacherModel.userId).ToList();
            }
        }

        [HttpPost]
        [ActionName("updatePassword")]
        public IHttpActionResult UpdatePassword(ChangePwdModel changePwd)
        {
            using (TutionProEntities context = new TutionProEntities())
            {
                // Check Old Password
                if (context.Users.Where(c => c.Password == changePwd.OldPassword).Count() == 0)
                {
                    return BadRequest("Old Password does not match our records!");
                }

                var user = context.Users.Where(c => c.UserKey.ToString().Equals(changePwd.UserKey)).FirstOrDefault();
                if (user != null)
                {
                    user.Password = changePwd.NewPassword;

                    context.Entry(user).State = EntityState.Modified;
                    context.SaveChanges();

                    // TO DO:

                    // Send email with confirmation

                    return Ok("success");
                }
                return BadRequest("error");
            }
        }

        [HttpGet]
        [ActionName("counters")]
        public IHttpActionResult GetCounters()
        {
            using (TutionProEntities context = new TutionProEntities())
            {
                CounterModel counterModel = new CounterModel();

                counterModel = context.Database
                   .SqlQuery<CounterModel>("exec [dbo].[GetCounters] ").FirstOrDefault();

                return Ok(counterModel);
            }
        }
    }
}
