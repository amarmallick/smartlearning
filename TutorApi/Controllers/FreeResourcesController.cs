﻿using ConnectSC.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace TutorApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class FreeResourcesController : ApiController
    {
        #region Free Resource
        [HttpPost]
        public IHttpActionResult UploadFile(string linkTitle, string linkUrl, int resourceTitleId)
        {
            string docPath = string.Empty;
            string docType = string.Empty;
            bool isDoc = false;
            FreeResource freeResource = new FreeResource();
            try
            {
                if (HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    // Get the uploaded image from the Files collection
                    var httpPostedFile = HttpContext.Current.Request.Files["UploadedImage"];

                    if (httpPostedFile != null)
                    {
                        // Validate the uploaded image(optional)

                        // Get the complete file path
                        var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/UploadedFiles"), httpPostedFile.FileName);

                        // Save the uploaded file to "UploadedFiles" folder
                        httpPostedFile.SaveAs(fileSavePath);
                        docPath = httpPostedFile.FileName;
                        docType = httpPostedFile.ContentType;
                        isDoc = true;
                    }
                }

                using (TutionProEntities context = new TutionProEntities())
                {
                    freeResource = new FreeResource
                    {
                        DocType = docType,
                        IsDoc = isDoc,
                        LinkTitle = linkTitle,
                        LinkUrl = linkUrl,
                        ResourceTitleID = resourceTitleId,
                        DocPath = docPath,
                    };
                    context.FreeResources.AddOrUpdate(c => c.ResourceId, freeResource);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                // log.Error(ex);
            }
            return Ok(freeResource);
        }

        [HttpGet]
        [ActionName("get-free-resources")]
        public IHttpActionResult GetFreeResources(int rtId)
        {
            using (var context = new TutionProEntities())
            {
                SqlParameter param = new SqlParameter("@ResourceTitleID", rtId);

                IEnumerable<FreeResource> custSub = context.Database
                    .SqlQuery<FreeResource>("SELECT * FROM [dbo].[FreeResource] WHERE ResourceTitleID = @ResourceTitleID", param);

                return Ok(custSub.ToList());
            }
        }

        [HttpGet]
        [ActionName("videos")]
        public IHttpActionResult GetVideos()
        {
            using (var context = new TutionProEntities())
            {
                //SqlParameter param = new SqlParameter("@ResourceTitleID", rtId);
                IEnumerable<FreeResource> custSub = context.Database
                    .SqlQuery<FreeResource>("SET ROWCOUNT 6; SELECT * FROM [dbo].[FreeResource] WHERE DocType = 'video/mp4' ORDER BY ResourceId Desc");

                return Ok(custSub.ToList());
            }
        }
        #endregion

        #region Resource Title
        [HttpPost]
        [ActionName("add-update-resource-title")]
        public IHttpActionResult AddUpTitle(ResourceTitle resourceTitle)
        {
            try
            {
                using (TutionProEntities context = new TutionProEntities())
                {
                    context.ResourceTitles.AddOrUpdate(c => c.ID, resourceTitle);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                // log.Error(ex);
            }
            return Ok(resourceTitle);
        }

        [HttpGet]
        [ActionName("get-resource-title")]
        public IHttpActionResult GetResourceTitles()
        {
            using (var context = new TutionProEntities())
            {
                var list = context.ResourceTitles;
                return Ok(list.ToList());
            }
        }
        #endregion
    }
}
