﻿using ConnectSC.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Linq;
using ConnectSC.Models;
using System.Data.SqlClient;

namespace TutorApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AssignmentsController : ApiController
    {
        [ActionName("add")]
        public void Add(Assignment assignment)
        {
            try
            {
                using (TutionProEntities context = new TutionProEntities())
                {
                    context.Assignments.AddOrUpdate(c => c.ID, assignment);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                // log.Error(ex);
            }
        }

        [ActionName("get-all")]
        public List<Assignment> Get()
        {
            using (TutionProEntities context = new TutionProEntities())
            {
                return context.Assignments.ToList();
            }
        }

        [HttpGet]
        [ActionName("search")]
        public List<Assignment> Search(string searchFilter)
        {
            using (TutionProEntities context = new TutionProEntities())
            {
                if(string.IsNullOrEmpty(searchFilter))
                {
                    return context.Assignments.ToList();
                }
                else
                {
                    searchFilter = searchFilter.ToLower();
                    return context.Assignments.Where(c => c.Title.ToLower().Contains(searchFilter)).ToList();
                }
                
            }
        }

        [ActionName("get-by-student")]
        public List<AssignmetModel> GetByStudent(int stuId)
        {
            using (TutionProEntities context = new TutionProEntities())
            {
                SqlParameter param = new SqlParameter("@StudentID", stuId);
                return context.Database.SqlQuery<AssignmetModel>("exec [dbo].[GetAssignmentsByStudent] @StudentID", param).ToList();
            }
        }

        [HttpGet]
        [ActionName("details")]
        public AssignmetModel Details(int id)
        {
            using (TutionProEntities context = new TutionProEntities())
            {
                SqlParameter param = new SqlParameter("@AssignmentID", id);
                return context.Database.SqlQuery<AssignmetModel>("exec [dbo].[GetAssignmentsDetails] @AssignmentID", param).FirstOrDefault();
            }
        }
    }
}