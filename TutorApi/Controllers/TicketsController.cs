﻿using ConnectSC.Data.Models;
using ConnectSC.Data.ViewModels;
using ConnectSC.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;


namespace TutorApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class TicketsController : ApiController
    {
        private TutionProEntities tutionProEntities = new TutionProEntities();


        [ActionName("get-all")]
        public List<ServiceTicket> Get()
        {
            return tutionProEntities.ServiceTickets.ToList();
        }

        [HttpGet]
        [ActionName("search")]
        public List<ServiceTicketViewModel> Search(string userId, int rowCount, string searchFilter)
        {
            using (TutionProEntities context = new TutionProEntities())
            {
                searchFilter = searchFilter ?? "";
                SqlParameter[] param = new SqlParameter[]
                {
                    new SqlParameter("@UserId", userId),
                    new SqlParameter("@SearchFilter", searchFilter),
                    new SqlParameter("@Rowcount", rowCount)
                };
                return context.Database.SqlQuery<ServiceTicketViewModel>("exec [dbo].[SearchTickets] @UserId, @SearchFilter, @Rowcount", param).ToList();
            }
        }

        [HttpPost]
        [ActionName("post")]
        public void Post(ServiceTicket serviceTicket, string userKey)
        {
            using (TutionProEntities tutionProEntities = new TutionProEntities())
            {
                DbContextTransaction transaction = tutionProEntities.Database.BeginTransaction();

                try
                {
                    tutionProEntities.ServiceTickets.Add(serviceTicket);
                    tutionProEntities.SaveChanges();

                    // Save Notification for User
                    var notification = new Notification
                    {
                        IsRead = false,
                        NGKey = Guid.NewGuid(),
                        NotificationText = "New Service Ticket raised.",
                        ShowFrom = DateTime.Now,
                        ShowTill = DateTime.Today.AddDays(5),
                        UserKey = Guid.Parse(userKey),
                    };
                    tutionProEntities.Notifications.Add(notification);
                    tutionProEntities.SaveChanges();

                    // Save Notification for Admin
                    notification = new Notification
                    {
                        IsRead = false,
                        NGKey = Guid.NewGuid(),
                        NotificationText = "New Service Ticket raised.",
                        ShowFrom = DateTime.Now,
                        ShowTill = DateTime.Today.AddDays(5),
                        UserKey = Guid.Parse("65C01D3B-4AA0-4151-8247-E1D817EF1618"),
                    };
                    tutionProEntities.Notifications.Add(notification);
                    tutionProEntities.SaveChanges();

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                }
            }
        }

        [HttpGet]
        [ActionName("get")]
        public ServiceTicketViewModel GetTicketDetails(int id)
        {
            using (TutionProEntities context = new TutionProEntities())
            {
                var serviceTicket = context.ServiceTickets.Find(id);
                var list = (from p in context.ServiceTicketComments
                            .OrderByDescending(c => c.RemarksDate)
                            join u in context.UserDetails on p.UserID equals u.UserID
                            select new ServiceTicketCommentsModel
                            {
                                ID = p.ID,
                                UserID = p.UserID,
                                Remarks = p.Remarks,
                                RemarksDate = p.RemarksDate,
                                ServiceTicketID = p.ServiceTicketID,
                                FullName = u.FirstName + " " + u.LastName
                            }).ToList();

                string name = string.Empty;
                var user = context.UserDetails.Where(x => x.UserID == serviceTicket.UserID).FirstOrDefault();
                if (user != null)
                {
                    name = string.Format("{0} {1}", user.FirstName, user.LastName);
                }

                ServiceTicketViewModel serviceTicketViewModel = new ServiceTicketViewModel
                {
                    ID = serviceTicket.ID,
                    Name = name,
                    Category = serviceTicket.Category,
                    Description = serviceTicket.Description,
                    TicketDate = serviceTicket.TicketDate,
                    Email = context.Users.Find(serviceTicket.UserID).UserName,
                    AttachmentPath = serviceTicket.AttachmentPath,
                    ServiceTicketComments = list
                };

                return serviceTicketViewModel;
            }
        }

        [HttpPost]
        [ActionName("add-ticket-comments")]
        public void Post(ServiceTicketComment serviceTicketComment)
        {
            using (TutionProEntities tutionProEntities = new TutionProEntities())
            {
                //DbContextTransaction transaction = tutionProEntities.Database.BeginTransaction();

                try
                {
                    serviceTicketComment.RemarksDate = DateTime.UtcNow;
                    tutionProEntities.ServiceTicketComments.Add(serviceTicketComment);
                    tutionProEntities.SaveChanges();

                    //transaction.Commit();
                }
                catch (Exception ex)
                {
                    //transaction.Rollback();
                }
            }
        }
    }
}
