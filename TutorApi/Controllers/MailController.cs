﻿using ConnectSmartConsulting.Models;
using System;
using System.Web;
using System.Configuration;
using System.IO;
using System.Web.Http;
using ConnectSC.Models;
using ConnectSmartConsulting.Helpers;
using ConnectSmartConsulting.Services;

namespace TutorApi.Controllers
{
    public class MailController : ApiController
    {
        #region Enquiry Mail
        [HttpPost]
        public IHttpActionResult ContactMail(ContactDetails ContactDetails)
        {
            string body = "";
          var  emailService = new EmailService();
            try
            {
                #region sending Enquiry mail to customer
                using (StreamReader reader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/Template/Enquiry.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{UserName}", ContactDetails.Name);

                EmailModel emailModel = new EmailModel
                {
                    Email = ContactDetails.Email,
                    Subject = "Thanks For Enquiry",
                    EmailBody = body
                };
                emailService.SendEmail(emailModel);

                #endregion

                #region sending Enquiry mail to Admin
                var Adminmail = ConfigurationManager.AppSettings["AdminMailID"];

                //streamReader will convert the html template
                using (StreamReader reader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/Template/AdminEnquiry.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{UserName}", ContactDetails.Name);
                body = body.Replace("{Service}", ContactDetails.SelectedService);
                body = body.Replace("{Email}", ContactDetails.Email);
                body = body.Replace("{Phone}", ContactDetails.Phone);
                body = body.Replace("{Subject}", ContactDetails.Subject);
                body = body.Replace("{Message}", ContactDetails.Message);

                emailModel = new EmailModel
                {
                    Email = Adminmail,
                    Subject = string.Format("{0} Enquiry", ContactDetails.SelectedService),
                    EmailBody = body
                };
                emailService.SendEmail(emailModel);
                #endregion
                ServiceEnquiry ServiceEnquiry = new ServiceEnquiry()
                {
                    Name = ContactDetails.Name,
                    Email = ContactDetails.Email,
                    Message = ContactDetails.Message,
                    Phone = ContactDetails.Phone,
                    Service = ContactDetails.SelectedService

                };

                HttpClientHelper<ServiceEnquiry> helper = new HttpClientHelper<ServiceEnquiry>();
                helper.Post(ServiceEnquiry, "users/postEnquiry");
            }
            catch (Exception ex)
            {
                // log.Error(string.Format("HomeController-ContactMail: {0}", ex));
                return BadRequest();
            }
            return Ok();
            //return RedirectToAction("Index");
        }
        #endregion

        [HttpPost]
        public IHttpActionResult Careers(CareerModel careerModel, HttpPostedFileBase fileUploader)
        {
            string body = "";
            var emailService = new EmailService();
            try
            {
                #region sending acknowledgement to user
                using (StreamReader reader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/Template/Careers.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{UserName}", careerModel.Name);
                body = body.Replace("{Position}", careerModel.Position);
                body = body.Replace("{Email}", careerModel.Email);
                body = body.Replace("{Phone}", careerModel.Phone);
                body = body.Replace("{Experience}", careerModel.Experience);
                body = body.Replace("{Designation}", careerModel.Designation);
                body = body.Replace("{Message}", "Job Application");

                EmailModel emailModel = new EmailModel
                {
                    Email = careerModel.Email,
                    Subject = "Thanks for applying to Connect Smart Consulting",
                    EmailBody = body
                };
                emailService.SendEmail(emailModel);

                #endregion

                #region sending Enquiry mail to Admin
                var Adminmail = ConfigurationManager.AppSettings["AdminMailID"];

                //streamReader will convert the html template
                using (StreamReader reader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/Template/Careers.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{UserName}", careerModel.Name);
                body = body.Replace("{Position}", careerModel.Position);
                body = body.Replace("{Email}", careerModel.Email);
                body = body.Replace("{Phone}", careerModel.Phone);
                body = body.Replace("{Experience}", careerModel.Experience);
                body = body.Replace("{Designation}", careerModel.Designation);
                body = body.Replace("{Message}", "New Job Application");

                emailModel = new EmailModel
                {
                    Email = Adminmail,
                    Subject = string.Format("Job Application: Connect Smart Consulting"),
                    EmailBody = body
                };
                if (fileUploader != null)
                {
                    emailService.SendEmail(emailModel, fileUploader);
                }
                emailService.SendEmail(emailModel);
                #endregion

            }
            catch (Exception ex)
            {
                //log.Error(string.Format("HomeController-ContactMail: {0}", ex));
                return BadRequest();
            }
            return Ok();
            //return RedirectToAction("Index");
        }
    }
}
