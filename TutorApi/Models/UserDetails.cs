﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TutorApi.Models
{
    public class UserDetails
    {
       
        public string Email { get; set; }
        public string Pass { get; set; }
        public string HighestEducation { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int PhoneNumber { get; set; }
        public string Address { get; set; }
        public int Role { get; set; }
        public string CurrentCity { get; set; }
        public string AreasYouTeach { get; set; }
        public string SubjectYouTeach { get; set; }
        public string TimeAvailibility { get; set; }
    }
}