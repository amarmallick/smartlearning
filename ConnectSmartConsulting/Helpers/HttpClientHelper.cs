﻿using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;

namespace ConnectSmartConsulting.Helpers
{
    public class HttpClientHelper<T> where T : class
    {
        #region Variables
        private static log4net.ILog Log { get; set; }
        ILog log = log4net.LogManager.GetLogger(typeof(HttpClientHelper<T>));
        private string uRL = ConfigurationManager.AppSettings["ApiUrl"].ToString();
        #endregion

        #region Get
        public List<T> Get(string path)
        {
            List<T> ViewModel = new List<T>();
            try
            {
                HttpClient client = new HttpClient
                {
                    BaseAddress = new Uri(string.Format("{0}/api", uRL))
                };
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:19.0) Gecko/20100101 Firefox/19.0");
                HttpResponseMessage response = client.GetAsync(path).Result;

                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    ViewModel = JsonConvert.DeserializeObject<List<T>>(result);
                }
            }
            catch (Exception ex)
            {
                log.Error(string.Format("HttpClientHelper-Get: {0}", ex));
                throw ex;
            }
            return ViewModel;
        }

        public T GetSingle(string path)
        {
            T ViewModel = null;
            try
            {
                HttpClient client = new HttpClient
                {
                    BaseAddress = new Uri(string.Format("{0}/api", uRL))
                };
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:19.0) Gecko/20100101 Firefox/19.0");
                HttpResponseMessage response = client.GetAsync(path).Result;

                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    ViewModel = JsonConvert.DeserializeObject<T>(result);
                }
            }
            catch (Exception ex)
            {
                log.Error(string.Format("HttpClientHelper-Get: {0}", ex));
                throw ex;
            }
            return ViewModel;
        }
        #endregion

        #region Post
        public bool Post(T details, string path)
        {
            try
            {
                var json = JsonConvert.SerializeObject(details);
                var buffer = System.Text.Encoding.UTF8.GetBytes(json);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                HttpClient client = new HttpClient
                {
                    BaseAddress = new Uri(string.Format("{0}/api", uRL))
                };
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:19.0) Gecko/20100101 Firefox/19.0");
                HttpResponseMessage response = client.PostAsync(path, byteContent).Result;
                return response.IsSuccessStatusCode;
            }
            catch (Exception ex)
            {
                log.Error(string.Format("HttpClientHelper-Post: {0}", ex));
                return false;
            }
        }
        #endregion
    }
}