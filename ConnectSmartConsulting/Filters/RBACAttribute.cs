﻿using ConnectSmartConsulting.Models;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ConnectSmartConsulting.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class RBACAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            /*Create permission string based on the requested controller 
              name and action name in the format 'controllername-action'*/
            var requiredPermission = string.Format("{0}-{1}",
                filterContext.ActionDescriptor.ControllerDescriptor.ControllerName,
                filterContext.ActionDescriptor.ActionName);

            /*Create an instance of our custom user authorisation object passing requesting 
              user's 'Windows Username' into constructor*/
            //RBACUser requestingUser = new RBACUser(filterContext.RequestContext
            //                                       .HttpContext.User.Identity.Name);


            var authCookie = HttpContext.Current.Session["LOGIN_USER"];
            //FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);

            if (authCookie == null)
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary
                    {
                        {"action", "login"},
                        {"controller", "SmartLearning"}
                    });
            }
            else
            {
                var user = (LoggedInUser)HttpContext.Current.Session["LOGIN_USER"];

                var requestingUser = new RBACUser(user.UserName, user.UserId);

                //Check if the requesting user has the permission to run the controller's action
                if (!requestingUser.HasPermission(requiredPermission))
                    filterContext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary
                        {
                            {"action", "Index"},
                            {"controller", "Unauthorised"}
                        });
            }
            /*If the user has the permission to run the controller's action, then 
              filterContext.Result will be uninitialized and executing the controller's 
              action is dependant on whether filterContext.Result is uninitialized.*/
        }
    }
}