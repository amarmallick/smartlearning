﻿using ConnectSmartConsulting.Models;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;

namespace ConnectSmartConsulting.Modules
{
    public class UserModule
    {
        #region Variables
        private static log4net.ILog Log { get; set; }
        ILog log = log4net.LogManager.GetLogger(typeof(UserModule));
        private string uRL = ConfigurationManager.AppSettings["ApiUrl"].ToString();

        public List<Users> UserVeiwModel { get; private set; }
        public List<UserDetails> UserDetailsVeiwModel { get; set; }
        #endregion

        public List<Users> GetAllUser()
        {
            List<Users> UserVeiwModel = new List<Users>();
            try
            {
                HttpClient client = new HttpClient
                {
                    BaseAddress = new Uri(uRL)
                };
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:19.0) Gecko/20100101 Firefox/19.0");
                HttpResponseMessage response = client.GetAsync("api/users/get-all").Result;

                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    UserVeiwModel = JsonConvert.DeserializeObject<List<Users>>(result);
                }

            }
            catch (Exception ex)
            {

                log.Error("Error Message in UserModule GetAllUser !" + ex);

            }
            return UserVeiwModel;
        }

        public LoggedInUser AuthenticateUser(string username, string password)
        {
            LoggedInUser loggedInUser = null;
            try
            {
                HttpClient client = new HttpClient
                {
                    BaseAddress = new Uri(uRL)
                };
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:19.0) Gecko/20100101 Firefox/19.0");

                HttpResponseMessage response = client.GetAsync("api/users/authenticate?username=" + username + "&password=" + password).Result;

                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    loggedInUser = JsonConvert.DeserializeObject<LoggedInUser>(result);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error Message in UserModule GetAllUser !" + ex);
            }
            return loggedInUser;
        }

        public bool AddUserDetails(UserDetails userDetails)
        {
            try
            {
                var json = JsonConvert.SerializeObject(userDetails);
                var buffer = System.Text.Encoding.UTF8.GetBytes(json);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpClient client = new HttpClient
                {
                    BaseAddress = new Uri(uRL)
                };
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:19.0) Gecko/20100101 Firefox/19.0");
                HttpResponseMessage response = client.PostAsync("api/Users/post", byteContent).Result;
                return response.IsSuccessStatusCode;
            }
            catch (Exception ex)
            {
                log.Error("Error Message in UserModule AddUserDetails !" + ex);
                return false;
            }
        }

        public bool HasPermission(string path)
        {
            bool pFound = false;
            try
            {
                HttpClient client = new HttpClient
                {
                    BaseAddress = new Uri(uRL)
                };
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:19.0) Gecko/20100101 Firefox/19.0");
                HttpResponseMessage response = client.GetAsync(path).Result;

                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    pFound = JsonConvert.DeserializeObject<bool>(result);
                }
            }
            catch (Exception ex)
            {
                log.Error(string.Format("UserModule-HasPermission {0}", ex.Message));
                throw ex;
            }
            return pFound;
        }
    }
}