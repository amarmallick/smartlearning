﻿using System;
using System.Web;
using System.Web.Optimization;

namespace ConnectSmartConsulting
{
    public class BundleConfig
    {
        public static void AddDefaultIgnorePatterns(IgnoreList ignoreList)
        {
            if (ignoreList == null)
                throw new ArgumentNullException("ignoreList");
            ignoreList.Ignore("*.intellisense.js");
            ignoreList.Ignore("*-vsdoc.js");
            ignoreList.Ignore("*.debug.js", OptimizationMode.WhenEnabled);
            //ignoreList.Ignore("*.min.js", OptimizationMode.WhenDisabled);
            ignoreList.Ignore("*.min.css", OptimizationMode.WhenDisabled);
        }

        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/theme_1/lib").Include(
                    "~/content/theme_1/lib/bootstrap/js/bootstrap.bundle.min.js",
                    "~/content/theme_1/lib/easing/easing.min.js",
                    "~/content/theme_1/lib/superfish/hoverIntent.js",
                    "~/content/theme_1/lib/superfish/superfish.min.js",
                    "~/content/theme_1/lib/wow/wow.min.js",
                    "~/content/theme_1/lib/waypoints/waypoints.min.js",
                    "~/content/theme_1/lib/counterup/counterup.min.js",
                    "~/content/theme_1/lib/owlcarousel/owl.carousel.min.js",
                    "~/content/theme_1/lib/isotope/isotope.pkgd.min.js",
                    //"~/content/theme_1/lib/lightbox/js/lightbox.min.js",
                    "~/content/theme_1/lib/touchSwipe/jquery.touchSwipe.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/theme_2/js").Include(
                   "~/content/theme_2/js/tether.min.js",
                   "~/content/theme_2/js/jquery.cookie.js",
                   "~/content/theme_2/js/grasp_mobile_progress_circle-1.0.0.min.js",
                   "~/content/theme_2/js/jquery.nicescroll.min.js",
                   "~/content/theme_2/js/jquery.validate.min.js",
                  // "~/content/theme_2/js/charts-home.js",
                   "~/content/theme_2/js/front.js"
                   ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                        "~/Content/font-awesome.min.css",
                        "~/Content/animate.css",
                        "~/Content/owl.carousel.css",
                        "~/Content/owl.theme.css",
                        "~/Content/toastr.css",
                        "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/theme_1/css/home_css").Include(
                        "~/content/theme_1/lib/animate/animate.min.css",
                        "~/content/theme_1/lib/owlcarousel/assets/owl.carousel.min.css",
                        //"~/content/theme_1/lib/lightbox/css/lightbox.min.css",
                        "~/content/theme_1/css/style.css"));

            bundles.Add(new StyleBundle("~/Content/theme_2/css/admin_css").Include(
                       "~/content/theme_2/css/style.default.css",
                       "~/content/theme_2/css/grasp_mobile_progress_circle-1.0.0.min.css",
                       "~/content/theme_2/css/custom.css",
                       "~/content/theme_2/css/toastr.min.css",
                       "~/content/theme_2/css/loading.css"));

           // BundleTable.EnableOptimizations = true;
        }
    }
}
