﻿using ConnectSmartConsulting.Modules;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConnectSmartConsulting.Models
{
    public class RBACUser
    {
        private readonly List<UserRole> Roles = new List<UserRole>();

        public RBACUser(string _username, int _userId)
        {
            User_Id = _userId;
            Username = _username;
            IsSysAdmin = false;
        }

        public int User_Id { get; set; }
        public bool IsSysAdmin { get; set; }
        public string Username { get; set; }


        public bool HasPermission(string requiredPermission)
        {
            var bFound = false;
            UserModule userModule = new UserModule();
            bFound = userModule.HasPermission(string.Format("api/users/has-permission?id={0}&permission={1}", User_Id, requiredPermission));
            return bFound;
        }

        public bool HasRole(string role)
        {
            return Roles.Where(p => p.RoleName.ToLower() == role.ToLower()).ToList().Count > 0;
        }

        public bool HasRoles(string roles)
        {
            var bFound = false;
            var _roles = roles.ToLower().Split(';');
            foreach (var role in Roles)
                try
                {
                    bFound = _roles.Contains(role.RoleName.ToLower());
                    if (bFound)
                        return bFound;
                }
                catch (Exception)
                {
                }
            return bFound;
        }
    }

    public class UserRole
    {
        public List<Permission> Permissions = new List<Permission>();
        public int Role_Id { get; set; }
        public string RoleName { get; set; }
    }

    public class Permission
    {
        public bool IsActive { get; set; }
        public int ID { get; set; }
        public string PermissionDescription { get; set; }
    }
}