﻿using ConnectSC.Data.Models;
using ConnectSC.Models;
using ConnectSmartConsulting.Helpers;
using ConnectSmartConsulting.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;
using ConnectSC.Data.ViewModels;

namespace ConnectSmartConsulting.Controllers
{
    public class ServiceRequestController : BaseController
    {
        [ActionName("add")]
        public ActionResult Add()
        {
            if (Session["LOGIN_USER"] != null)
            {
                var usr = (LoggedInUser)Session["LOGIN_USER"];
                return View(usr);
            }
            return RedirectToAction("Login", "SmartLearning");
        }

        [HttpPost]
        [ActionName("add")]
        public ActionResult Add(ServiceTicket model)
        {
            var usr = (LoggedInUser)Session["LOGIN_USER"];

            var serviceTicket = new ServiceTicket
            {
                Category = model.Category,
                Description = model.Description,
                UserID = usr.UserId,
                TicketDate = DateTime.UtcNow.AddHours(5).AddMinutes(30),
                AttachmentPath = string.Empty
            };

            HttpClientHelper<ServiceTicket> helper = new HttpClientHelper<ServiceTicket>();
            helper.Post(serviceTicket, "tickets/post?userKey=" + LoggedInUser.UserKey);

            return RedirectToAction("index");
        }

        [ActionName("index")]
        public ActionResult Index(string searchFilter)
        {
            HttpClientHelper<ServiceTicketViewModel> helper = new HttpClientHelper<ServiceTicketViewModel>();

            if (Session["LOGIN_USER"] != null)
            {
                var userData = (LoggedInUser)Session["LOGIN_USER"];

                searchFilter = searchFilter ?? "";
                var tickets = helper.Get(string.Format("tickets/search?userId={0}&rowCount=0&searchFilter={1}", userData.UserId, searchFilter));

                ViewBag.SearchFilter = searchFilter;
                return View("index", tickets);
            }

            return RedirectToAction("login", new { msg = "loggedout" });
        }

        [ActionName("show")]
        public ActionResult Show(int? id)
        {
            if (Session["LOGIN_USER"] != null)
            {
                var usr = (LoggedInUser)Session["LOGIN_USER"];
                return View(usr);
            }
            return RedirectToAction("Login", "SmartLearning");
        }
    }
}