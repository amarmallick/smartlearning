﻿
using ConnectSC.Data.Models;
using ConnectSC.Models;
using ConnectSmartConsulting.Filters;
using ConnectSmartConsulting.Helpers;
using ConnectSmartConsulting.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ConnectSmartConsulting.Controllers
{
    //[RBAC]
    public class AdminController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        #region Tutor
        [ActionName("add-tutor")]
        public ActionResult AddTutor()
        {
            return View("AddTutor");
        }

        [ActionName("add-tutor")]
        [HttpPost]
        public ActionResult AddTutor(UserDetails userDetails)
        {
            try
            {
                if (ValidateMailByEmail(userDetails.Email))
                {
                    userDetails.Role = 2;
                    var data = userModule.AddUserDetails(userDetails);
                    if (data)
                    {
                        string subject = "Registered  Successfully.";
                        string body = "Thanks for registering with us.";
                        SendRegisterMail(userDetails.Email, subject, body, userDetails.FirstName, userDetails.Pass);

                        return RedirectToAction("tutors");
                    }
                }
                else
                {
                    ViewBag.Registration = "Email is Already Registered.";
                    ViewBag.Result = "Error";
                }
            }
            catch (Exception ex)
            {
                log.Error("Error Message in Admin Controller AddTutor !" + ex);
            }
            return View("AddTutor");
        }

        [ActionName("edit-tutor")]
        public ActionResult EditTutor(int? id)
        {
            HttpClientHelper<Users> helper = new HttpClientHelper<Users>();
            var user = helper.GetSingle(string.Format("users/get?id={0}", id));

            return View("EditTutor", user);
        }

        [ActionName("edit-tutor")]
        [HttpPost]
        public ActionResult EditTutor(UserDetails userDetails)
        {
            HttpClientHelper<UserDetails> helper = new HttpClientHelper<UserDetails>();
            helper.Post(userDetails, "users/update-user");

            return RedirectToAction("tutors");
        }

        [ActionName("approve-tutor")]
        [HttpPost]
        public ActionResult ApproveTutor(string UserID, string Status, string Reason)
        {
            HttpClientHelper<AproveUser> httpClientHelper = new HttpClientHelper<AproveUser>();
            AproveUser user = new AproveUser
            {
                IsApproved = (Status == "True") ? true : false,
                Reason = "test",
                UserID = Convert.ToInt32(UserID)
            };
            var success = httpClientHelper.Post(user, "users/update");

            // Notify User of the Approval
            NotifyApproval(UserID, user, success);

            return RedirectToAction("tutors");
        }

        [ActionName("tutors")]
        public ActionResult TutorView()
        {
            UsersViewModel usersViewModel = new UsersViewModel();
            try
            {

                var data = userModule.GetAllUser();
                usersViewModel.Users = data.Where(m => m.Role.Equals(2)).ToList();

                foreach (var item in usersViewModel.Users)
                {
                    item.ActiveStatus = item.IsActive ? "Yes" : "No";
                    item.ApprovedStatus = item.IsApproved ? "Yes" : "No";
                }

                var helperTutor = new HttpClientHelper<TutorModel>();
                ViewBag.Students = helperTutor.Get(string.Format("students/get-all"));
                //ViewBag.AssignedStudents = helperTutor.Get(string.Format("users/GetStudentTeacherMapping"));
            }
            catch (Exception ex)
            {
                log.Error("Error Message in Admin controller TutorView !" + ex);
            }
            return View("TutorView", usersViewModel);
        }

        [ActionName("assign-students")]
        [HttpPost]
        public ActionResult AssignStudents(int[] Student,int UserID)
        {
            // code to assign students  POST /api/Users/addStudentTeacherMapping

            StudentTeacherModel studentTeacherModel = new ConnectSC.Models.StudentTeacherModel();
            studentTeacherModel.userIds = Student.ToList();
            studentTeacherModel.userId = UserID;
            HttpClientHelper<StudentTeacherModel> httpClientHelper = new HttpClientHelper<StudentTeacherModel>();

            var data = httpClientHelper.Post(studentTeacherModel, "users/addStudentTeacherMapping");

            return Json("",JsonRequestBehavior.AllowGet);
        }

        [ActionName("get-studentsByTutor")]
        [HttpGet]
        public ActionResult GetStudentsByTutor(int TutorId)
        {
             HttpClientHelper<TutorModel> helperTutor = new HttpClientHelper<TutorModel>();
            var list = helperTutor.Get(string.Format("users/getStudentTeacherMapping?userId={0}", TutorId));

            return Json(list.Select(m => m.UserId).ToArray(), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Student
        [ActionName("add-student")]
        public ActionResult AddStudent()
        {
            return View("AddStudent");
        }

        [ActionName("add-student")]
        [HttpPost]
        public ActionResult AddStudent(UserDetails userDetails)
        {
            try
            {
                if (ValidateMailByEmail(userDetails.Email))
                {
                    userDetails.Role = 3;
                    var data = userModule.AddUserDetails(userDetails);
                    if (data)
                    {
                        string subject = "Registrated Successfully.";
                        string Name = userDetails.FirstName;
                        string Pass = userDetails.LastName;
                        string body = "Thanks for registering with us.";
                        SendRegisterMail(userDetails.Email, subject, body, Name, Pass);
                        ViewBag.Registration = "Success";

                        return RedirectToAction("students");
                    }
                }
                else
                {
                    ViewBag.Registration = "Email is Already Registered.";
                    ViewBag.Result = "Error";
                }
            }
            catch (Exception ex)
            {
                log.Error(string.Format("AdminController-AddStudent: {0}", ex));
            }
            return View("AddStudent");
        }

        [ActionName("edit-student")]
        public ActionResult EditStudent(int? id)
        {
            HttpClientHelper<Users> helper = new HttpClientHelper<Users>();
            var user = helper.GetSingle(string.Format("users/get?id={0}", id));

            var list = new List<TutorModel>();
            HttpClientHelper<TutorModel> helperTutor = new HttpClientHelper<TutorModel>();
            list = helperTutor.Get(string.Format("users/getStudentTeacherMapping?userId={0}", id));


            return View("EditStudent", user);
        }

        [ActionName("edit-student")]
        [HttpPost]
        public ActionResult EditStudent(UserDetails userDetails)
        {
            HttpClientHelper<UserDetails> helper = new HttpClientHelper<UserDetails>();
            helper.Post(userDetails, "users/update-user");

            return RedirectToAction("students");
        }

        [ActionName("approve-student")]
        [HttpPost]
        public ActionResult ApproveStudent(string UserID, string Status, string Reason)
        {
            HttpClientHelper<AproveUser> httpClientHelper = new HttpClientHelper<AproveUser>();
            AproveUser user = new AproveUser
            {
                IsApproved = (Status == "True") ? true : false,
                Reason = Reason,
                UserID = Convert.ToInt32(UserID)
            };
            var success = httpClientHelper.Post(user, "users/update");

            // Notify User of the Approval
            NotifyApproval(UserID, user, success);

            return RedirectToAction("students");
        }

        private void NotifyApproval(string UserID, AproveUser user, bool success)
        {
            if (success)
            {
                // Send Approval Email
                if (user.IsApproved == true)
                {
                    HttpClientHelper<Users> helper = new HttpClientHelper<Users>();
                    var userDetails = helper.GetSingle(string.Format("users/get?id={0}", UserID));

                    //***********TESTING*********//
                    //userDetails.UserName = "chauhan.munish1@gmail.com";
                    //***********END TESTING*********//

                    SendApprovalMail(userDetails.UserName,
                        "Your Smartlearning Account is now active",
                        string.Empty,
                        string.Format("{0} {1}", userDetails.UserDetails[0].FirstName, userDetails.UserDetails[0].LastName),
                        userDetails.Password);
                }
            }
        }

        [ActionName("students")]
        public ActionResult StudentView()
        {
            UsersViewModel usersViewModel = new UsersViewModel();
            try
            {
                var data = userModule.GetAllUser();
                usersViewModel.Users = data.Where(m => m.Role.Equals(3)).ToList();

                foreach (var item in usersViewModel.Users)
                {
                    item.ActiveStatus = item.IsActive == true ? "Yes" : "No";
                    item.ApprovedStatus = item.IsApproved == true ? "Yes" : "No";
                }
            }
            catch (Exception ex)
            {
                log.Error(string.Format("AdminController-StudentView: {0}", ex));
            }
            return View("StudentView", usersViewModel);
        }
        #endregion

        #region Resources
        [ActionName("resources")]
        public ActionResult Resources()
        {
            return View("Resources");
        }

        [ActionName("add-resource")]
        public ActionResult AddResource()
        {
            return View("AddResource");
        }

        [ActionName("add-resource")]
        [HttpPost]
        public ActionResult AddResource(HttpPostedFileBase file)
        {
            try
            {
                if (file.ContentLength > 0)
                {
                    String ftpServerIP = ConfigurationManager.AppSettings["ftpServerIP"];
                    String ftpUserID = ConfigurationManager.AppSettings["ftpUserID"];
                    String ftpPassword = ConfigurationManager.AppSettings["ftpPassword"];

                    // String filePath = "D://taj.png";
                    var filePath = Path.GetFileName(file.FileName);
                    String ext = new FileInfo(filePath).Extension;
                    // Let's create dynamically a filename from datetime
                    string dynamicFileName = String.Format("{0:yyyyMMdd_HHmmssms}{1}", DateTime.Now, ext);

                    String ftpFilePath = String.Format("Documents/{0}", dynamicFileName);
                    FtpWebRequest ftpRequest;
                    ftpRequest = (FtpWebRequest)WebRequest.Create(
                      String.Format("{0}/{1}", ftpServerIP, ftpFilePath));
                    ftpRequest.Method = WebRequestMethods.Ftp.UploadFile;
                    ftpRequest.Credentials = new NetworkCredential(ftpUserID, ftpPassword);

                    // get file bytes
                    byte[] fileBytes = null;
                    using (StreamReader fileStream = new StreamReader(file.InputStream))
                    {
                        fileBytes = Encoding.UTF8.GetBytes(fileStream.ReadToEnd());
                        fileStream.Close();
                    }
                    ftpRequest.ContentLength = fileBytes.Length;
                    Stream requestStream = ftpRequest.GetRequestStream();
                    requestStream.Write(fileBytes, 0, fileBytes.Length);
                    requestStream.Close();

                    //    var fileName = Path.GetFileName(file.FileName);
                    //   var path = Path.Combine(Server.MapPath("~/Documents/uploads"), fileName);
                    //  file.SaveAs(path);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error Message in Admin controller AddResource (HttpPost Attribute) !" + ex);
            }
            return View();
        }
        #endregion

        #region ServiceEnquiry
        [ActionName("service-enquiry")]
        public ActionResult ServiceEnquiry()
        {
            ServiceEnquiryList serviceEnquiryList = new ServiceEnquiryList();

            try
            {
                HttpClientHelper<ServiceEnquiry> helper = new HttpClientHelper<ServiceEnquiry>();
                serviceEnquiryList.ServiceList = helper.Get(string.Format("users/getEnquiry"));
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
            return View("ServiceEnquiry", serviceEnquiryList);
        }

        [ActionName("sr-details")]
        public ActionResult SRDetails(int? eid)
        {
            return View("SRDetails");
        }
        #endregion

        #region StudentSubscription
        [ActionName("add-subscription")]
        public ActionResult AddSubscription()
        {
            return View("AddSubscription");
        }

        #region Subscriptions
        [ActionName("subscriptions")]
        public ActionResult StudentSubscription()

        {
            UsersViewModel usersViewModel = new UsersViewModel();
            usersViewModel.Subscription = new List<SubscriptionModel>();
            usersViewModel.Users = new List<Users>();
            usersViewModel.UserSubscriptionDetails = new List<UserSubscriptionModel>();

            try
            {
                //Packages
                HttpClientHelper<SubscriptionModel> helper = new HttpClientHelper<SubscriptionModel>();
                usersViewModel.Subscription = helper.Get(string.Format("users/getSubscriptionPackage"));
                // Subscriptions
                HttpClientHelper<UserSubscriptionModel> helperUser = new HttpClientHelper<UserSubscriptionModel>();
                usersViewModel.UserSubscriptionDetails = helperUser.Get(string.Format("users/getUserSubscriptionDetails"));

                var helperTutor = new HttpClientHelper<TutorModel>();
                usersViewModel.Tutors = helperTutor.Get(string.Format("tutors/get-all"));

                //usersViewModel.Users = (from d in usersViewModel.Users
                //             join u in userSubscription on d.UserId equals u.UserId into t
                //             from res in t.DefaultIfEmpty() 
                //             join s in usersViewModel.Subscription on res.SubscriptionId equals s.SubscriptionId 
                //             select new Users()
                //             {
                //                 UserId=d.UserId,
                //                 UserName=d.UserName,
                //                 SubscriptionPackage = s.SubcriptionName,
                //                 SubscriptionStartDate = res != null ? res.SubscriptionStartDate.ToString(): null,
                //                 SubscriptionEndDate = res != null ?  res.SubscriptionEndDate.ToString(): null

                //             }).ToList();

                //usersViewModel.Users = (from d in userSubscription
                //                        join u in usersViewModel.Users on d.UserId equals u.UserId into r
                //                        from res in r.DefaultIfEmpty()
                //                        join s in usersViewModel.Subscription on d.SubscriptionId equals s.SubscriptionId
                //                        select new Users()
                //                        {
                //                            UserName = res.UserName,
                //                            SubscriptionPackage = s.SubcriptionName != null ? s.SubcriptionName.ToString() : "",
                //                            SubscriptionStartDate = d.SubscriptionStartDate != null ? d.SubscriptionStartDate.ToString() : "",
                //                            SubscriptionEndDate = d.SubscriptionEndDate != null ? d.SubscriptionEndDate.ToString() : "",
                //                            UserDetails = res.UserDetails

                //                        }).ToList();

                //     var loj = (from prsn in usersViewModel.Users
                //join co in userSubscription on prsn.UserId equals co.UserId into comps
                //from y in comps.DefaultIfEmpty()
                //join prod in db.Products on prsn.Person_ID equals prod.Person_ID into prods
                //from x in prods.DefaultIfEmpty()
                //select new { Person = prsn.NAME, Company = y.NAME, Product = x.NAME })



            }
            catch (Exception ex)
            {
                log.Error(string.Format("AdminController-StudentView: {0}", ex));
            }
            return View("StudentSubscription", usersViewModel);
        }
        #endregion

        [ActionName("subscriptions")]
        [HttpPost]
        public ActionResult Subscriptions(string UserID, string SubStartDate, string SubEndDate, string Subscription, string[] Tutor)
        {
            UserSubscription UserSubscriptionModel = new UserSubscription()
            {
                SubscriptionStartDate = DateTime.Parse(SubStartDate),
                SubscriptionEndDate = DateTime.Parse(SubEndDate),
                UserId = int.Parse(UserID),
                SubscriptionId = int.Parse(Subscription),
                Tutors = string.Join(",", Tutor.ToArray())
            };
            HttpClientHelper<UserSubscription> httpClientHelper = new HttpClientHelper<UserSubscription>();

            var data = httpClientHelper.Post(UserSubscriptionModel, "users/AddUserSubscriptionDetails");

            return RedirectToAction("subscriptions");
        }
        #endregion

        #region Non Action
        [NonAction]
        private Boolean ValidateMailByEmail(string email)
        {
            var userList = userModule.GetAllUser();
            var users = userList.Where(x => x.UserName.Equals(email)).Count();

            if (users > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        [NonAction]
        void SendRegisterMail(string email, string subject, string body, string Name, string pass)
        {
            try
            {
                using (StreamReader reader = new StreamReader(Server.MapPath("~/Template/RegisterUser.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{Email}", email);
                body = body.Replace("{Pass}", pass);
                body = body.Replace("{UserName}", Name);

                EmailModel emailModel = new EmailModel
                {
                    Email = email,
                    Subject = subject,
                    EmailBody = body
                };
                emailService.SendEmail(emailModel);
            }
            catch (Exception ex)
            {
                log.Error(string.Format("SmartLearningController-SendRegisterMail: {0}", ex));
            }
        }

        [NonAction]
        void SendApprovalMail(string email, string subject, string body, string Name, string pass)
        {
            try
            {
                using (StreamReader reader = new StreamReader(Server.MapPath("~/Template/Approval.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{Email}", email);
                body = body.Replace("{Pass}", pass);
                body = body.Replace("{UserName}", Name);

                EmailModel emailModel = new EmailModel
                {
                    Email = email,
                    Subject = subject,
                    EmailBody = body
                };
                emailService.SendEmail(emailModel);
            }
            catch (Exception ex)
            {
                log.Error(string.Format("SmartLearningController-SendApprovalMail: {0}", ex));
            }
        }
        #endregion

        //#region ServiceEnquiry
        //public ActionResult ServiceEnquiry()
        //{
        //    ServiceEnquiryList serviceEnquiryList = new ServiceEnquiryList();

        //    try
        //    {
        //        HttpClientHelper<ServiceEnquiry> helper = new HttpClientHelper<ServiceEnquiry>();
        //        serviceEnquiryList.ServiceList = helper.Get(string.Format("users/getEnquiry"));

        //    }
        //    catch (Exception ex)
        //    {

        //     log.Error(ex);
        //    }

        //    return View(serviceEnquiryList);
        //}
        //#endregion

        [ActionName("dash")]
        public ActionResult DashboardRedirect(string uk)
        {
            var loggedIn = (LoggedInUser)(Session["LOGIN_USER"]);
            uk = uk ?? loggedIn.UserKey;

            if (!string.IsNullOrEmpty(uk))
            {
                HttpClientHelper<LoggedInUser> helper = new HttpClientHelper<LoggedInUser>();
                LoggedInUser loggedInUser = helper.GetSingle(string.Format("users/get-user?uk={0}", uk));

                if (loggedInUser == null)
                    return RedirectToAction("login", new { msg = "loggedout" });

                Session["LOGIN_USER"] = loggedInUser;

                switch (loggedInUser.Role)
                {
                    case (int)URole.Admin:
                        return RedirectToAction("index", "admin");
                    case (int)URole.Tutor:
                        return RedirectToAction("index", "tutor");
                    case (int)URole.Student:
                        return RedirectToAction("index", "student");
                }
                return RedirectToAction("login", new { msg = "loggedout" });
            }
            return RedirectToAction("login", new { msg = "loggedout" });
        }

        #region Session
        [ActionName("sessions")]
        public ActionResult Sessions()
        {
            HttpClientHelper<MeetingModel> helper = new HttpClientHelper<MeetingModel>();
            var meetings = helper.Get(string.Format("meetings/get-all"));

            return View("Sessions", meetings);
        }

        [ActionName("add-session")]
        public ActionResult AddSession()
        {
            return View("AddSession");
        }
        #endregion
    }
}