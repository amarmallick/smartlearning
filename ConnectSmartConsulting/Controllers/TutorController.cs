﻿using System.Collections.Generic;
using ConnectSC.Models;
using ConnectSmartConsulting.Filters;
using ConnectSmartConsulting.Helpers;
using ConnectSmartConsulting.Models;
using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ConnectSC.Data.Models;

namespace ConnectSmartConsulting.Controllers
{
    //[RBAC]
    public class TutorController : BaseController
    {
        // GET: Tutor
        public ActionResult Index()
        {
            if (Session["LOGIN_USER"] == null)
            {
                return RedirectToAction("login", "SmartLearning");
            }

            var userDet = (LoggedInUser)Session["LOGIN_USER"];
            int? id = userDet.UserId;

            HttpClientHelper<Users> helper = new HttpClientHelper<Users>();
            var user = helper.GetSingle(string.Format("users/get?id={0}", id));
            return View("Index", user);
        }

        #region Profile
        [ActionName("my-profile")]
        public ActionResult MyProfile()
        {
            if (Session["LOGIN_USER"] == null)
            {
                return RedirectToAction("login", "SmartLearning");
            }

            var userDet = (LoggedInUser)Session["LOGIN_USER"];
            int? id = userDet.UserId;

            HttpClientHelper<Users> helper = new HttpClientHelper<Users>();
            var user = helper.GetSingle(string.Format("users/get?id={0}", id));
            return View("MyProfile", user);
        }

        [ActionName("edit-profile")]
        public ActionResult EditProfile()
        {
            if (Session["LOGIN_USER"] == null)
            {
                return RedirectToAction("login", "SmartLearning");
            }

            var userDet = (LoggedInUser)Session["LOGIN_USER"];
            int? id = userDet.UserId;

            HttpClientHelper<Users> helper = new HttpClientHelper<Users>();
            var user = helper.GetSingle(string.Format("users/get?id={0}", id));
            return View("EditProfile", user);
        }

        [ActionName("edit-profile")]
        [HttpPost]
        public ActionResult EditProfile(UserDetails userDetails, HttpPostedFileBase file)
        {
            string dynamicFileName = SaveImage(file);

            HttpClientHelper<UserDetails> helper = new HttpClientHelper<UserDetails>();
            userDetails.ImagePath = dynamicFileName;
            helper.Post(userDetails, "users/update-user");

            return RedirectToAction("my-profile");
        }


        #endregion

        [ActionName("students")]
        public ActionResult Students(string searchFilter)
        {
            var userDet = (LoggedInUser)Session["LOGIN_USER"];
            int? id = userDet.UserId;

            var list = new List<TutorModel>();
            HttpClientHelper<TutorModel> helper = new HttpClientHelper<TutorModel>();
            //list = helper.Get(string.Format("users/getStudentTeacherMapping?userId={0}", id));
            list = helper.Get(string.Format("students/search?searchFilter={0}", searchFilter));
            ViewBag.SearchFilter = searchFilter;

            return View("Students", list);
        }

        [ActionName("student-profile")]
        public ActionResult ViewStudentProfile(int? id)
        {
            HttpClientHelper<Users> helper = new HttpClientHelper<Users>();
            var user = helper.GetSingle(string.Format("users/get?id={0}", id));
            return View("ViewStudentProfile", user);
        }

        [ActionName("schedule-session")]
        public ActionResult ScheduleSession()
        {
            return View("ScheduleSession");
        }

        [ActionName("create-schedule")]
        public ActionResult CreateSchedule()
        {
            return View("CreateSchedule");
        }

        [ActionName("join-session")]
        public ActionResult JoinSession()
        {
            return View("JoinSession");
        }

        [ActionName("calendar")]
        public ActionResult Calendar()
        {
            return View("Calendar");
        }

        [ActionName("upload-notes")]
        public ActionResult UploadNotes()
        {
            return View("UploadNotes");
        }

        #region Assignments
        [ActionName("add-assignment")]
        public ActionResult AddAssignment()
        {
            var helperTutor = new HttpClientHelper<TutorModel>();
            var list = helperTutor.Get(string.Format("students/get-by-tutor?tutorId=" + LoggedInUser.UserId));

            list.Insert(0, new TutorModel { UserId = -1, FullName = "All" });
            ViewBag.Students = list;

            return View("AddAssignment");
        }

        [HttpPost]
        [ActionName("add-assignment")]
        public ActionResult AddAssignment(Assignment assignment, HttpPostedFileBase file)
        {
            string fileName = string.Empty;
            fileName = SaveFile(file);
            // assignment Attachment Path
            assignment.Attachment = fileName;

            string d = Request["DueDate"];
            // assignment Due Date
            assignment.DueDate = DateTime.Parse(d);

            string students = Request["Students"];
            assignment.UserId = students;

            assignment.CreatedBy = LoggedInUser.UserId;
            assignment.CreatedOn = DateTime.UtcNow;
            assignment.IsActive = true;

            HttpClientHelper<Assignment> httpClientHelper = new HttpClientHelper<Assignment>();
            var data = httpClientHelper.Post(assignment, "assignments/add");

            return RedirectToAction("view-assignments");
        }

        [ActionName("view-assignments")]
        public ActionResult ViewAssignments(string searchFilter)
        {
            List<Assignment> assignment = new List<Assignment>();
            try
            {
                HttpClientHelper<Assignment> helper = new HttpClientHelper<Assignment>();
                ViewBag.SearchFilter = searchFilter;
                assignment = helper.Get(string.Format("assignments/search?searchFilter={0}", searchFilter));
            }
            catch (Exception ex)
            {
                log.Error("Error Message in Tutor controller view-assignments !" + ex);
            }
            return View("ViewAssignments", assignment);
        }

        public FileResult Download(string filePath)
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(Server.MapPath("/documents/uploads/" + filePath));
            string fileName = filePath;
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }
        #endregion

        [ActionName("publish-result")]
        public ActionResult PublishResult()
        {
            return View("PublishResult");
        }

        [ActionName("create-free-material")]
        public ActionResult CreateFreeMaterial()
        {
            return View("CreateFreeMaterial");
        }

        [NonAction]
        private string SaveImage(HttpPostedFileBase file)
        {
            string dynamicFileName = string.Empty;
            try
            {
                if (file.ContentLength > 0)
                {
                    dynamicFileName = DateTime.Now.Ticks + Path.GetExtension(file.FileName);
                    var path = Path.Combine(Server.MapPath("~/Documents/uploads"), dynamicFileName);
                    file.SaveAs(path);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error Message in Tutor controller SaveImage!!" + ex);
            }

            return dynamicFileName;
        }

        [NonAction]
        private string SaveFile(HttpPostedFileBase file)
        {
            string dynamicFileName = string.Empty;
            try
            {
                if (file != null)
                {
                    if (file.ContentLength > 0)
                    {
                        dynamicFileName = DateTime.Now.Ticks + Path.GetExtension(file.FileName);
                        var path = Path.Combine(Server.MapPath("~/Documents/uploads"), dynamicFileName);
                        file.SaveAs(path);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error Message in Student controller SaveImage!" + ex);
            }

            return dynamicFileName;
        }
    }
}