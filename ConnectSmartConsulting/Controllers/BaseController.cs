﻿using ConnectSmartConsulting.Helpers;
using ConnectSmartConsulting.Models;
using ConnectSmartConsulting.Modules;
using ConnectSmartConsulting.Services;
using log4net;
using System.Web.Mvc;

namespace ConnectSmartConsulting.Controllers
{
    public class BaseController : Controller
    {
        #region Variables
        protected static log4net.ILog Log { get; set; }
        protected ILog log = log4net.LogManager.GetLogger(typeof(BaseController));
        protected UserModule userModule = new UserModule();
        protected IEmailService emailService;
        #endregion

        public BaseController()
        {
            emailService = new EmailService();
        }

        public LoggedInUser LoggedInUser
        {
            get
            {
                if (Session["LOGIN_USER"] != null)
                {
                    LoggedInUser loggedInUser = (LoggedInUser)Session["LOGIN_USER"];
                    return loggedInUser;
                }
                return null;
            }
        }
    }
}