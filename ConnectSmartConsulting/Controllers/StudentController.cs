﻿using ConnectSC.Models;
using ConnectSmartConsulting.Filters;
using ConnectSmartConsulting.Helpers;
using ConnectSmartConsulting.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace ConnectSmartConsulting.Controllers
{
    //[RBAC]
    public class StudentController : BaseController
    {
        // GET: Student
        public ActionResult Index()
        {
            if (Session["LOGIN_USER"] == null)
            {
                return RedirectToAction("login", "SmartLearning");
            }

            var userDet = (LoggedInUser)Session["LOGIN_USER"];
            int? id = userDet.UserId;

            HttpClientHelper<Users> helper = new HttpClientHelper<Users>();
            var user = helper.GetSingle(string.Format("users/get?id={0}", id));
            return View("Index", user);
        }

        [ActionName("sessions")]
        public ActionResult Sessions()
        {
            return View("Sessions");
        }

        [ActionName("my-sessions")]
        public ActionResult MySessions()
        {
            return View("MySessions");
        }

        [ActionName("subscriptions")]
        public ActionResult Subscriptions()
        {
            return View("Subscriptions");
        }

        [ActionName("tutors")]
        public ActionResult Tutors()
        {
             var userDet = (LoggedInUser)Session["LOGIN_USER"];
            int? id = userDet.UserId;

            var list = new List<TutorModel>();
            HttpClientHelper<TutorModel> helper = new HttpClientHelper<TutorModel>();
            list = helper.Get(string.Format("users/getStudentTeacherMapping?userId={0}", id));
            return View("Tutors", list);
        }

        [ActionName("assignments")]
        public ActionResult Assignments()
        {
            return View("Assignments");
        }

        [ActionName("show-assignment")]
        public ActionResult ShowAssignment(int? id)
        {
            return View("ShowAssignment");
        }

        [ActionName("submit-assignment")]
        public ActionResult SubmitAssignment(int? id)
        {
            return View("SubmitAssignment");
        }

        [ActionName("submit-assignment")]
		[HttpPost]
		public ActionResult Assignments(SubmitAssignment submitAssignment)
		{
			if (Request.Files.Count > 0)
			{
				HttpFileCollectionBase files = Request.Files;
				string dynamicFileName = SaveImage(files[0]);
				var userDet = (LoggedInUser) Session["LOGIN_USER"];
				submitAssignment.UserId = userDet.UserId;
				var helper = new HttpClientHelper<SubmitAssignment>();
				submitAssignment.Attachment = dynamicFileName;
				helper.Post(submitAssignment, "students/SubmitAssignment");
			}
			return Json("Assignment Has Been Successfully Uploaded",JsonRequestBehavior.AllowGet);
		}

        [ActionName("resources")]
        public ActionResult Resources()
        {
            return View("Resources");
        }

        [ActionName("search-tutors")]
        public ActionResult SearchTutors()
        {
            var list = new List<TutorModel>();
            HttpClientHelper<TutorModel> helper = new HttpClientHelper<TutorModel>();
            list = helper.Get("tutors/search");

            return View("SearchTutors", list);
        }

        [ActionName("tutor-profile")]
        public ActionResult TutorProfile(int? id)
        {
            HttpClientHelper<Users> helper = new HttpClientHelper<Users>();
            var user = helper.GetSingle(string.Format("users/get?id={0}", id));
            return View("TutorProfile", user);
        }

        #region My Profile
        [ActionName("my-profile")]
        public ActionResult MyProfile()
        {
            if (Session["LOGIN_USER"] == null)
            {
                return RedirectToAction("login", "SmartLearning");
            }

            var userDet = (LoggedInUser)Session["LOGIN_USER"];
            int? id = userDet.UserId;

            HttpClientHelper<Users> helper = new HttpClientHelper<Users>();
            var user = helper.GetSingle(string.Format("users/get?id={0}", id));
            return View("MyProfile", user);
        }

        [ActionName("edit-profile")]
        public ActionResult EditProfile()
        {
            if (Session["LOGIN_USER"] == null)
            {
                return RedirectToAction("login", "SmartLearning");
            }

            var userDet = (LoggedInUser)Session["LOGIN_USER"];
            int? id = userDet.UserId;

            HttpClientHelper<Users> helper = new HttpClientHelper<Users>();
            var user = helper.GetSingle(string.Format("users/get?id={0}", id));
            return View("EditProfile", user);
        }

        [ActionName("edit-profile")]
        [HttpPost]
        public ActionResult EditProfile(UserDetails userDetails, HttpPostedFileBase file)
        {
            string dynamicFileName = SaveImage(file);

            HttpClientHelper<UserDetails> helper = new HttpClientHelper<UserDetails>();
            userDetails.ImagePath = dynamicFileName;
            helper.Post(userDetails, "users/update-user");

            return RedirectToAction("my-profile");
        }
        #endregion

        [NonAction]
        private string SaveImage(HttpPostedFileBase file)
        {
            string dynamicFileName = string.Empty;
            try
            {
                if (file.ContentLength > 0)
                {
                    dynamicFileName = DateTime.Now.Ticks + Path.GetExtension(file.FileName);
                    var path = Path.Combine(Server.MapPath("~/Documents/uploads"), dynamicFileName);
                    file.SaveAs(path);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error Message in Student controller SaveImage!" + ex);
            }

            return dynamicFileName;
        }
    }
}