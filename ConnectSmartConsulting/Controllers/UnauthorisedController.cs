﻿using System.Web.Mvc;

namespace ConnectSmartConsulting.Controllers
{
    public class UnauthorisedController : Controller
    {
        // GET: Unauthorised
        public ActionResult Index()
        {
            return View();
        }
    }
}