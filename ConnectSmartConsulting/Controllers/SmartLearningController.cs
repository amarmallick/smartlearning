﻿using ConnectSC.Models;
using ConnectSmartConsulting.Helpers;
using ConnectSmartConsulting.Models;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace ConnectSmartConsulting.Controllers
{
    public class SmartLearningController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        #region Login
        public ActionResult Login(string msg)
        {
            if (!string.IsNullOrEmpty(msg))
            {
                if(msg.Equals("register_success"))
                {
                    ViewBag.Message = "You have been registered successfully, but your account pending approval. Kindly check your inbox for updates.";
                }
                else
                {
                    ViewBag.Message = "You have been logged out successfully";
                }
            }
            return View();
        }

        [HttpPost]
        public ActionResult Login(Users user)
        {
            try
            {
                //var data = userModule.GetAllUser();
                //var selectedData = data.Where(m => m.UserName.Equals(user.UserName)).FirstOrDefault();
                var selectedData = userModule.AuthenticateUser(user.UserName, user.Password);

                if (selectedData != null)
                {
                    if (selectedData.IsApproved == false)
                    {
                        ViewBag.Registration = "Your account is pending approval. Kindly check your inbox after some time.";
                            //"You may contact administrator - admin@connectsmartconsulting.com";
                        return View();
                    }
                    if (selectedData.IsActive.Equals(true) && selectedData.IsApproved.Equals(true))
                    {
                        Session["LOGIN_USER"] = selectedData;

                        switch (selectedData.Role)
                        {
                            case (int)URole.Admin:
                                return RedirectToAction("index", "admin");
                            case (int)URole.Tutor:
                                return RedirectToAction("index", "tutor");
                            case (int)URole.Student:
                                return RedirectToAction("index", "student");
                        }
                    }
                    else
                    {
                        ViewBag.Registration = "User is not approved or activated";
                        ViewBag.Result = "Error";
                    }
                }
                else
                {
                    ViewBag.Registration = "Email Or Password is invalid";
                    ViewBag.Result = "Error";

                }
            }
            catch (Exception ex)
            {
                log.Error("Error Message in SmartLearning Controller Login !" + ex);
            }
            return View();
        }
        #endregion

        #region Register
        [ActionName("need-a-tutor")]
        public ActionResult Register()
        {
            return View("Register");
        }

        [HttpPost]
        public ActionResult Register(UserDetails userDetails)
        {
            try
            {
                if (ValidateMailByEmail(userDetails.Email))
                {
                    userDetails.Role = 3;
                    var data = userModule.AddUserDetails(userDetails);
                    if (data)
                    {
                        string subject = "Registrated Successfully.";
                        string Name = userDetails.FirstName;
                        string Pass = userDetails.LastName;
                        string body = "Thanks for registering with us.";
                        SendRegisterMail(userDetails.Email, subject, body, Name, Pass);
                    }
                    return RedirectToAction("Login", new { msg = "register_success" });
                }
                else
                {
                    ViewBag.Registration = "Email is Already Registered.";
                    ViewBag.Result = "Error";
                }
            }
            catch (Exception ex)
            {
                log.Error("Error Message in SmartLearning Controller Register !" + ex);
            }
            return View();
        }
        #endregion

        #region Forgot Password
        public ActionResult ForgotPassword()
        {
            ViewBag.Result = " ";
            return View();
        }

        [HttpPost]
        public ActionResult ForgotPassword(string Email)
        {
            try
            {
                var password = System.Web.Security.Membership.GeneratePassword(8, 1);
                string subject = "Forgot Password.";
                string body = "Your Password for ConnectSmartConsulting is:  " + password;
                SendForgotPasswordMail(Email, subject, password);
                ViewBag.Result = "Message";

            }
            catch (Exception ex)
            {
                log.Error("Error Message in SmartLearning Controller ForgotPassword !" + ex);
            }

            return View();
        }
        #endregion

        #region Change Password
        [ActionName("change-password")]
        public ActionResult ChangePassword()
        {
            if (Session["LOGIN_USER"] == null)
                return RedirectToAction("login", new { msg = "log_out" });

            return View("ChangePassword");
        }
        #endregion

        #region Tutor Registration
        [ActionName("tutor-registration")]
        public ActionResult TutorRegister()
        {
            return View("TutorRegister");
        }

        [HttpPost]
        public ActionResult TutorRegister(UserDetails userDetails)
        {
            try
            {
                if (ValidateMailByEmail(userDetails.Email))
                {
                    userDetails.Role = 2;
                    var data = userModule.AddUserDetails(userDetails);
                    if (data)
                    {
                        string subject = "Registered  Successfully.";
                        string body = "Thanks for registering with us.";
                        SendRegisterMail(userDetails.Email, subject, body, userDetails.FirstName, userDetails.Pass);
                    }
                    return RedirectToAction("Login", new { msg = "register_success" });
                }
                else
                {
                    ViewBag.Registration = "Email is Already Registered.";
                    ViewBag.Result = "Error";
                }
            }
            catch (Exception ex)
            {
                //log.Debug("Debug Message!");
                //log.Warn("Warn Message!");
                log.Error("Error Message in SmartLearning Controller TutorRegister !" + ex);
                // log.Fatal("Fatal Message!");
                // throw ex;
            }
            return View();

        }
        #endregion

        #region Free Resources
        [ActionName("free-resources")]
        public ActionResult FreeResource()
        {
            //if (HttpContext.Cache["email"] == null)
            //{
            //    ViewBag.email = null;
            //}
            return View("FreeResource");
        }

        [HttpPost]
        public ActionResult FreeResource(UserDetails userDetails)
        {
            if (HttpContext.Cache["email"] == null)
            {
                HttpContext.Cache["email"] = userDetails.Email;
            }

            ViewBag.email = HttpContext.Cache["email"];
            return View();
        }
        #endregion

        #region Smart Team
        [ActionName("smart-team")]
        public ActionResult SmartTeam()
        {
            return View("SmartTeam");
        }

        #endregion

        #region Logout
        public ActionResult Logout()
        {
            Session["LOGIN_USER"] = null;
            Session.Remove("LOGIN_USER");
            Session.Abandon();

            return RedirectToAction("login", new { msg = "loggedout" });
        }
        #endregion

        //public JsonResult GetAllCountries()
        //{
        //    var objDict = new Dictionary<string, string>();
        //    try
        //    {

        //        foreach (var cultureInfo in CultureInfo.GetCultures(CultureTypes.SpecificCultures))
        //        {
        //            var regionInfo = new RegionInfo(cultureInfo.Name);
        //            if (!objDict.ContainsKey(regionInfo.EnglishName))
        //            {
        //                objDict.Add(regionInfo.EnglishName, regionInfo.TwoLetterISORegionName.ToLower());
        //            }
        //        }
        //        var obj = objDict.OrderBy(p => p.Key).ToArray();


        //    }
        //    catch (Exception ex)
        //    {

        //        log.Error("Error Message in SmartLearning Controller GetAllCountries !" + ex);

        //    }
        //    return Json(obj.Select(t => new
        //    {
        //        Text = t.Key,
        //        Value = t.Value
        //    }), JsonRequestBehavior.AllowGet);
        //}

        #region Contact
        public ActionResult SmartLearningContact()
        {
            ContactDetails ContactDetails = new ContactDetails
            {
                SelectedService = "Smart Learning"
            };
            return View(ContactDetails);
        }

        [HttpPost]
        public ActionResult SmartLearningContact(ContactDetails ContactDetails)
        {
            string body = "";
            try
            {
                #region sending Enquiry mail to customer
                using (StreamReader reader = new StreamReader(Server.MapPath("~/Template/Enquiry.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{UserName}", ContactDetails.Name);

                EmailModel emailModel = new EmailModel
                {
                    Email = ContactDetails.Email,
                    Subject = "Thanks For Enquiry",
                    EmailBody = body
                };
                emailService.SendEmail(emailModel);

                #endregion

                #region sending Enquiry mail to Admin
                var Adminmail = ConfigurationManager.AppSettings["AdminMailID"];

                //streamReader will convert the html template
                using (StreamReader reader = new StreamReader(Server.MapPath("~/Template/AdminEnquiry.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{UserName}", ContactDetails.Name);
                body = body.Replace("{Service}", ContactDetails.SelectedService);
                body = body.Replace("{Email}", ContactDetails.Email);
                body = body.Replace("{Phone}", ContactDetails.Phone);
                body = body.Replace("{Subject}", ContactDetails.Subject);
                body = body.Replace("{Message}", ContactDetails.Message);

                emailModel = new EmailModel
                {
                    Email = Adminmail,
                    Subject = string.Format("{0} Enquiry", ContactDetails.SelectedService),
                    EmailBody = body
                };
                emailService.SendEmail(emailModel);
                #endregion
                ServiceEnquiry ServiceEnquiry = new ServiceEnquiry()
                {
                    Name = ContactDetails.Name,
                    Email = ContactDetails.Email,
                    Message = ContactDetails.Message,
                    Phone = ContactDetails.Phone,
                    Service = ContactDetails.SelectedService

                };

                HttpClientHelper<ServiceEnquiry> helper = new HttpClientHelper<ServiceEnquiry>();
                helper.Post(ServiceEnquiry, "users/postEnquiry");
                ViewBag.Message = "Thanks For your Time. We will be in touch with you soon.";
            }
            catch (Exception ex)
            {
                log.Error(string.Format("HomeController-ContactMail: {0}", ex));
            }
            return View();
            //return RedirectToAction("Index");
        }
        #endregion

        #region Non Action
        [NonAction]
        private Boolean ValidateMailByEmail(string email)
        {
            var userList = userModule.GetAllUser();
            var users = userList.Where(x => x.UserName.Equals(email)).Count();

            if (users > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        [NonAction]
        void SendForgotPasswordMail(string email, string subject, string password)
        {
            try
            {
                string body = null;
                //streamReader will convert the html template
                using (StreamReader reader = new StreamReader(Server.MapPath("~/Template/ForgotPassword.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{UserName}", email);
                body = body.Replace("{Password}", password);

                EmailModel emailModel = new EmailModel
                {
                    Email = email,
                    Subject = subject,
                    EmailBody = body
                };
                emailService.SendEmail(emailModel);
            }
            catch (Exception ex)
            {
                log.Error(string.Format("SmartLearningController-SendForgotPasswordMail: {0}", ex));
            }
        }

        [NonAction]
        void SendRegisterMail(string email, string subject, string body, string Name, string pass)
        {
            try
            {
                using (StreamReader reader = new StreamReader(Server.MapPath("~/Template/RegisterUser.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{Email}", email);
                body = body.Replace("{Pass}", pass);
                body = body.Replace("{UserName}", Name);

                EmailModel emailModel = new EmailModel
                {
                    Email = email,
                    Subject = subject,
                    EmailBody = body
                };
                emailService.SendEmail(emailModel);
            }
            catch (Exception ex)
            {
                log.Error(string.Format("SmartLearningController-SendRegisterMail: {0}", ex));
            }
        }
        #endregion

        #region Navigation
        public PartialViewResult _nav()
        {
            if (Session["LOGIN_USER"] != null)
            {
                var userData = (LoggedInUser)Session["LOGIN_USER"];

                switch (userData.Role)
                {
                    case (int)URole.Admin:
                        return PartialView("_adminNav");
                    case (int)URole.Tutor:
                        return PartialView("_tutorNav");
                    case (int)URole.Student:
                        return PartialView("_studentNav");
                }
            }

            return PartialView("_emptyNav");
        }
        #endregion

        #region Notifications
        [ActionName("notifications")]
        public ActionResult Notifications()
        {
            return View("Notifications");
        }
        #endregion

        public ActionResult Registration()
        {

            return View();
        }
        public ActionResult StudentRegistration()
        {

            return View();
        }

    }
}