﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ConnectSmartConsulting.Controllers
{
    public class FreeResourcesController : Controller
    {
        [ActionName("index")]
        public ActionResult Index()
        {
            return View("index");
        }

        [ActionName("resource-links")]
        public ActionResult ResourceLinks()
        {
            return View("ResourceLinks");
        }


        [ActionName("add")]
        public ActionResult Add()
        {
            return View("add");
        }
    }
}