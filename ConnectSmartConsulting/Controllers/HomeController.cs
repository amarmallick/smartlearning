﻿using ConnectSC.Models;
using ConnectSmartConsulting.Helpers;
using ConnectSmartConsulting.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace ConnectSmartConsulting.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            ContactDetails contactDetails = new ContactDetails();
            try
            {
                contactDetails.Service = new List<string>()
                { "Software Resellers",
                  "Smart Learning",
                  "Digital Consulting",
                  "Sales & Digital Marketing",
                  "ERP Consulting",
                  "Business Consultants",
                  "Application Support & Maintenance",
                  "IT Stratagy & Consulting"
                };

            }
            catch (Exception ex)
            {
                log.Error("Error Message in Home controller Index !" + ex);
            }

            return View(contactDetails);
        }

        #region Capabilities
        [ActionName("software-reseller")]
        public ActionResult SoftwareReseller()
        {
            ContactDetails ContactDetails = new ContactDetails
            {
                SelectedService = "Software Resellers"
            };
            return View("SoftwareReseller", ContactDetails);
        }

        [ActionName("smart-learning")]
        public ActionResult SmartLearning()
        {
            return View("SmartLearning");
        }

        [ActionName("digital-consulting")]
        public ActionResult DigitalConsulting()
        {
            ContactDetails ContactDetails = new ContactDetails
            {
                SelectedService = "Digital Consulting"
            };
            return View("DigitalConsulting", ContactDetails);
        }

        [ActionName("sales-and-digital-marketing")]
        public ActionResult SalesDigiMarketing()
        {
            ContactDetails ContactDetails = new ContactDetails
            {
                SelectedService = "Sales & Digital Marketing"
            };
            return View("SalesDigiMarketing", ContactDetails);
        }

        [ActionName("erp-consulting")]
        public ActionResult ERPConsulting()
        {
            ContactDetails ContactDetails = new ContactDetails
            {
                SelectedService = "ERP Consulting"
            };
            return View("ERPConsulting", ContactDetails);
        }

        [ActionName("business-consultants")]
        public ActionResult BusinessConsultants()
        {
            ContactDetails ContactDetails = new ContactDetails
            {
                SelectedService = "Business Consultants"
            };
            return View("BusinessConsultants", ContactDetails);
        }

        [ActionName("application-support-maintenance")]
        public ActionResult ApplicationSupportAndMaint()
        {
            ContactDetails ContactDetails = new ContactDetails
            {
                SelectedService = "Application Support & Maintenance"
            };
            return View("ApplicationSupportAndMaint", ContactDetails);
        }

        [ActionName("it-consulting")]
        public ActionResult ITConsulting()
        {
            ContactDetails ContactDetails = new ContactDetails
            {
                SelectedService = "IT Stratagy & Consulting"
            };
            return View("ITConsulting", ContactDetails);
        }
        [ActionName("smart-jobs")]
        public ActionResult SmartJobs()
        {
            ContactDetails ContactDetails = new ContactDetails
            {
                SelectedService = "Smart Jobs"
            };
            return View("SmartJobs");
        }
        [ActionName("software-Engineering")]
        public ActionResult SoftwareEngineering()
        {
            ContactDetails ContactDetails = new ContactDetails
            {
                SelectedService = "Software Engineering"
            };
            return View("SoftwareEngineering", ContactDetails);
        }

        #endregion

        #region Contact
        public ActionResult Contact()
        {
            ContactDetails contactdetails = PrepareContactModel();
            return View(contactdetails);
        }

        private static ContactDetails PrepareContactModel()
        {
            return new ContactDetails
            {
                Service = new List<string>()
                {
                  "Smart Learning",
                  "Smart Jobs",
                  "Smart Engineering (Agile Practices)",
                  "Software Resellers",
                  "Sales & Digital Marketing",
                  "ERP Consulting",
                  "Application Support & Maintenance"
                }
            };
        }
        #endregion

        #region Enquiry Mail
        [HttpPost]
        public ActionResult ContactMail(ContactDetails ContactDetails)
        {
            string body = "";
            try
            {
                #region sending Enquiry mail to customer
                using (StreamReader reader = new StreamReader(Server.MapPath("~/Template/Enquiry.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{UserName}", ContactDetails.Name);

                EmailModel emailModel = new EmailModel
                {
                    Email = ContactDetails.Email,
                    Subject = "Thanks For Enquiry",
                    EmailBody = body
                };
                emailService.SendEmail(emailModel);

                #endregion

                #region sending Enquiry mail to Admin
                var Adminmail = ConfigurationManager.AppSettings["AdminMailID"];

                //streamReader will convert the html template
                using (StreamReader reader = new StreamReader(Server.MapPath("~/Template/AdminEnquiry.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{UserName}", ContactDetails.Name);
                body = body.Replace("{Service}", ContactDetails.SelectedService);
                body = body.Replace("{Email}", ContactDetails.Email);
                body = body.Replace("{Phone}", ContactDetails.Phone);
                body = body.Replace("{Subject}", ContactDetails.Subject);
                body = body.Replace("{Message}", ContactDetails.Message);

                emailModel = new EmailModel
                {
                    Email = Adminmail,
                    Subject = string.Format("{0} Enquiry", ContactDetails.SelectedService),
                    EmailBody = body
                };
                emailService.SendEmail(emailModel);
                #endregion
                ServiceEnquiry ServiceEnquiry = new ServiceEnquiry()
                {
                    Name = ContactDetails.Name,
                    Email = ContactDetails.Email,
                    Message = ContactDetails.Message,
                    Phone = ContactDetails.Phone,
                    Service = ContactDetails.SelectedService

                };

                HttpClientHelper<ServiceEnquiry> helper = new HttpClientHelper<ServiceEnquiry>();
                helper.Post(ServiceEnquiry, "users/postEnquiry");
                ViewBag.Message = "Thanks For your Time. We will be in touch with you soon.";
            }
            catch (Exception ex)
            {
                log.Error(string.Format("HomeController-ContactMail: {0}", ex));
            }
            ContactDetails contactdetails = PrepareContactModel();
            return View("Contact", contactdetails);
            //return RedirectToAction("Index");
        }
        #endregion

        public ActionResult Team()
        {
            return View("Team");
        }

        public ActionResult Careers()
        {
            return View("Careers");
        }

        [HttpPost]
        public ActionResult Careers(CareerModel careerModel, HttpPostedFileBase fileUploader)
        {
            string body = "";
            try
            {
                #region sending acknowledgement to user
                using (StreamReader reader = new StreamReader(Server.MapPath("~/Template/Careers.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{UserName}", careerModel.Name);
                body = body.Replace("{Position}", careerModel.Position);
                body = body.Replace("{Email}", careerModel.Email);
                body = body.Replace("{Phone}", careerModel.Phone);
                body = body.Replace("{Experience}", careerModel.Experience);
                body = body.Replace("{Designation}", careerModel.Designation);
                body = body.Replace("{Message}", "Job Application");

                EmailModel emailModel = new EmailModel
                {
                    Email = careerModel.Email,
                    Subject = "Thanks for applying to Connect Smart Consulting",
                    EmailBody = body
                };
                emailService.SendEmail(emailModel);

                #endregion

                #region sending Enquiry mail to Admin
                var Adminmail = ConfigurationManager.AppSettings["AdminMailID"];

                //streamReader will convert the html template
                using (StreamReader reader = new StreamReader(Server.MapPath("~/Template/Careers.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{UserName}", careerModel.Name);
                body = body.Replace("{Position}", careerModel.Position);
                body = body.Replace("{Email}", careerModel.Email);
                body = body.Replace("{Phone}", careerModel.Phone);
                body = body.Replace("{Experience}", careerModel.Experience);
                body = body.Replace("{Designation}", careerModel.Designation);
                body = body.Replace("{Message}", "New Job Application");

                emailModel = new EmailModel
                {
                    Email = Adminmail,
                    Subject = string.Format("Job Application: Connect Smart Consulting"),
                    EmailBody = body
                };
                if (fileUploader != null)
                {
                    emailService.SendEmail(emailModel, fileUploader);
                }
                emailService.SendEmail(emailModel);
                #endregion

                ViewBag.Message = "Your application has been submitted successfully. Thanks for applying to Connect Smart Consulting.";
            }
            catch (Exception ex)
            {
                log.Error(string.Format("HomeController-ContactMail: {0}", ex));
            }
            ContactDetails contactdetails = PrepareContactModel();
            return View("Careers", contactdetails);
            //return RedirectToAction("Index");
        }

        public ActionResult JobDetails(string job)
        {
            ViewBag.JobTitle = job;
            return View("JobDetails");
        }

        public ActionResult ComingSoon()
        {
            return View("ComingSoon");
        }

        public JsonResult ServerPing()
        {
            return Json(DateTime.Now, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Image(string img)
        {
            var dir = Server.MapPath(@"\documents\uploads");
            var path = Path.Combine(dir, img);

            if (!System.IO.File.Exists(path))
            {
                img = "no_image_available.jpeg";
                path = Path.Combine(dir, img);
            }

            return File(path, "image/png");
        }
    }
}