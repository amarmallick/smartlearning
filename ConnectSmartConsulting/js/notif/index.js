﻿(function ($) {
    "use strict";

    var notifications = {
        loadNotif: function () {
            loadTable();
        }
    }

    $(document).ready(function () {
        // init
        // load Notif list
        notifications.loadNotif();
    });
}(jQuery));

function del(ngKey) {
    var result = confirm('Are you sure?');
    if (result) {
        var jqxhr = $.post(remoteURL + "notifications/delete?ngKey=" + ngKey, null)
            .done(function (data) {
                // Load Resources
                loadTable();
            }).fail(function (xhr) {
                console.log(xhr);
                showErrors(xhr);
            })
    }
}

function loadTable() {
    $('#list-notif').dataTable({
        destroy: true,
        searching: false,
        "paging": false,
        //"info": false,
        "ajax": {
            "url": remoteURL + "notifications/get-all",
            //"beforeSend": function (xhr) {
            //    xhr.setRequestHeader("Authorization", "Basic " + Cookies.get('auth'));
            //},
            "dataSrc": ""
        },
        "columns": [
            {
                "data": "NotificationText", "sortable": false
            },
            {
                "data": "ShowFrom", "sortable": false,
                "render": function (data) {
                    return moment(data).format('DD/MM/YYYY');
                }
            },
            {
                "data": "ShowTill", "sortable": false,
                "render": function (data) {
                    return moment(data).format('DD/MM/YYYY');
                }
            },
            {
                "data": null, "sortable": false, "render": function (url, type, full) {
                    return '<div class="pull-right"><button class="btn btn-xs btn-danger" onclick="del(\'' + full.NGKey + '\')">Delete</button></div>';
                }
            }
        ],


    });
}

