﻿(function ($) {
    "use strict";
    var user = {
        // update password
        updatePwd: function (form) {
            var json = JSON.parse(toJSONString(form));

            var jqxhr = $.post(baseURL + "/users/authenticate?username=" + json.UserName + "&password=" + json.Password, null)
                .done(function (data) {
                    if (data.UserKey != "") {
                        localStorage.ukey = data.UserKey;
                        //toastr.success('Authentication successful. Redirecting to Dashboard');
                        window.location.href = "/admin/dash?uk=" + data.UserKey;
                    }
                    else
                        toastr.error('There was an error. Please retry');
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    var m = jqXHR.responseJSON.Message;

                    if (m == "invalid") {
                        toastr.error('Invalid login credentials');
                    }
                    else if (m == "not_approved") {
                        toastr.error('Your account is pending approval. Kindly try after some time. You may contact administrator-admin@connectsmartconsulting.com');
                    }
                    else {
                        toastr.error('There was an error. Please retry');
                    }
                })
        },
    }

    $(document).ready(function () {
        $("#msform").on("submit", function (event) {
            event.preventDefault();
            user.updatePwd(this);
        });
    });
}(jQuery));




