﻿(function ($) {
    "use strict";

    var resources = {
        loadLinks: function () {
            loadTable();
        }
    }

    // init
    $(document).ready(function () {
        // load links list
        resources.loadLinks();
    });
}(jQuery));

function loadTable() {
    $('#list-links').dataTable({
        destroy: true,
        searching: false,
        "paging": false,
        "info": false,
        "ajax": {
            "url": baseURL + "/FreeResources/get-free-resources?rtId=1",
            "dataSrc": ""
        },
        "columns": [
            {
                "data": "LinkTitle", "sortable": false
            },
            {
                "data": "LinkUrl", "sortable": false
            },
            {
                "data": "DocType", "sortable": false
            },
            {
                "data": "IsDoc", "sortable": false, "render": function (url, type, full) {
                    if (full.IsDoc) {
                        return '<div class="text-right"><button class="btn btn-xs btn-info" onclick="download(\'' + full.DocPath + '\')">Download</button></div>'
                    }
                    else {
                        return '<div class="text-right"><button class="btn btn-xs" onclick="redirect(\'' + full.LinkUrl + '\')" target="_blank">View HTML</button></div>'
                    };
                }
            }
        ]
    });
}

function download(linkUrl) {
    //alert(linkUrl);
    window.open('http://localhost:51307/content/images/' + linkUrl);
}

function redirect(link) {
    window.open('http://'+ link, '_blank');
}