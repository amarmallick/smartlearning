﻿(function ($) {
    "use strict";
    var myMeeting = {
        // add meeting
        addMeeting: function (form) {
            var json = JSON.parse(toJSONString(form));

            var res = json.StartTime.split(" ");

            var dataToPost = {
                TutorID: json.Tutors,
                StudentID: json.Students,
                MeetingName: json.MeetingName,
                MeetingUrl: json.MeetingUrl,
                StartDateTime: json.StartDate + " " + res[0],
                EndDateTime: json.StartDate + " " + res[1],
            };

            console.log(dataToPost);

            var jqxhr = $.post(baseURL + "/meetings/add", dataToPost)
                .done(function (data) {
                    toastr.success('Session created');

                    timedRedirect('/admin/sessions', 2000);
                }).fail(function (xhr) {
                    console.log(xhr);
                    showErrors(xhr);
                })
        },

        // load Tutors
        loadTutors: function () {
            var jqxhr = $.get(baseURL + "/tutors/get-all")
                .done(function (data) {
                    var $tutors = $("#Tutors");
                    var html = '';
                    for (var i = 0, len = data.length; i < len; ++i) {
                        html = html + '<option value="' + data[i]['UserId'] + '">' + data[i]['FullName'] + '(' + data[i]['UserName'] + ')' + '</option>';
                    }
                    $tutors.append(html);
                })
                .fail(function (err) {
                    console.log(err);
                })
        },
        // load Students
        loadStudents: function () {
            var jqxhr = $.get(baseURL + "/students/get-all")
                .done(function (data) {
                    var $students = $("#Students");
                    var html = '';
                    for (var i = 0, len = data.length; i < len; ++i) {
                        html = html + '<option value="' + data[i]['UserId'] + '">' + data[i]['FullName'] + '(' + data[i]['UserName'] + ')' + '</option>';
                    }
                    $students.append(html);
                })
                .fail(function (err) {
                    console.log(err);
                })
        },
    }

    $(document).ready(function () {
        $("#form-session").on("submit", function (event) {
            event.preventDefault();
            myMeeting.addMeeting(this);
        });

        // load tutors
        myMeeting.loadTutors();
        // load students
        myMeeting.loadStudents();


        $(function () {
            $("#Tutors")
                .on('click', function () {
                    $("#TutorID").val($('#Tutors :selected').val());
                });
        });

        $(function () {
            $("#Students")
                .on('click', function () {
                    $("#StudentID").val($('#Students :selected').val());
                });
        });
    });
}(jQuery));




