﻿(function ($) {
    "use strict";
    var user = {
        // update password
        updatePwd: function (form) {
            var json = JSON.parse(toJSONString(form));

            if (json.NewPassword != json.ConfirmPassword) {
                toastr.warning('Password and Confirm Password should match');
                return;
            };

            var jqxhr = $.post(baseURL + "/users/updatePassword", json)
                .done(function (data) {
                    if (data == "success")
                        toastr.success('Password successfully updated');
                    else
                        toastr.error('There was an error. Please retry');
                }).fail(function (xhr) {
                    showErrors(xhr);
                })
        },
    }

    $(document).ready(function () {
        $("#myprofile").addClass("active");

        $("#frmCP").on("submit", function (event) {
            event.preventDefault();
            user.updatePwd(this);
        });
    });
}(jQuery));




