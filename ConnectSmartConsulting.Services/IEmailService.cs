﻿using ConnectSmartConsulting.Models;
using System.Web;

namespace ConnectSmartConsulting.Services
{
    public interface IEmailService
    {
        void SendEmail(EmailModel emailModel);
        void SendEmail(EmailModel emailModel, HttpPostedFileBase httpPostedFileBase);
    }
}
