﻿using ConnectSmartConsulting.Models;
using System.IO;
using System.Net.Mail;
using System.Web;

namespace ConnectSmartConsulting.Services
{
    public class EmailService : IEmailService
    {
        public void SendEmail(EmailModel emailModel)
        {
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("webmail.connectsmartconsulting.com");
            //streamReader will convert the html template

            mail.From = new MailAddress("support@connectsmartconsulting.com");
            mail.To.Add(emailModel.Email);
            mail.Subject = emailModel.Subject;
            mail.Body = emailModel.EmailBody;
            mail.IsBodyHtml = true;

            SmtpServer.Port = 25;
            SmtpServer.Credentials = new System.Net.NetworkCredential("support@connectsmartconsulting.com", "Shahzad123$");
            SmtpServer.EnableSsl = false;

            SmtpServer.Send(mail);
        }

        public void SendEmail(EmailModel emailModel, HttpPostedFileBase fileUploader)
        {
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("webmail.connectsmartconsulting.com");
            //streamReader will convert the html template

            mail.From = new MailAddress("support@connectsmartconsulting.com");
            mail.To.Add(emailModel.Email);
            mail.Subject = emailModel.Subject;
            mail.Body = emailModel.EmailBody;
            mail.IsBodyHtml = true;

            string fileName = Path.GetFileName(fileUploader.FileName);
            mail.Attachments.Add(new Attachment(fileUploader.InputStream, fileName));

            SmtpServer.Port = 25;
            SmtpServer.Credentials = new System.Net.NetworkCredential("support@connectsmartconsulting.com", "Shahzad123$");
            SmtpServer.EnableSsl = false;

            SmtpServer.Send(mail);
        }
    }
}
